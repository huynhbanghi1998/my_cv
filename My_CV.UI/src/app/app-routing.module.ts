import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ThemeComponent } from './theme/theme.component';
import { AuthGuard } from './common/auth.guard';
import { InformationComponent } from './pages/information/information.component';
import { AccountComponent } from './pages/account/account.component';
import { ExperienceDetailComponent } from './pages/experience/experience-detail/experience-detail.component';
import { ExperienceOverviewComponent } from './pages/experience/experience-overview/experience-overview.component';
import { ExperienceResolver } from './pages/experience/@service/experience.resolver';
import { LanguageDetailComponent } from './pages/language/language-detail/language-detail.component';
import { LanguageOverviewComponent } from './pages/language/language-overview/language-overview.component';
import { LanguageResolver } from './pages/language/@service/language.resolver';
import { SkillDetailComponent } from './pages/skill/skill-detail/skill-detail.component';
import { SkillOverviewComponent } from './pages/skill/skill-overview/skill-overview.component';
import { SkillResolver } from './pages/skill/@service/skill.resolver';
import { ProductDetailComponent } from './pages/product/product-detail/product-detail.component';
import { ProductOverviewComponent } from './pages/product/product-overview/product-overview.component';
import { ProductResolver } from './pages/product/@service/product.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  {
    path: 'dsb', component: ThemeComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'information', pathMatch: 'full' },
      { path: 'information', component: InformationComponent },
      { path: 'account', component: AccountComponent },
      { path: 'experience/experience-overview', component: ExperienceOverviewComponent },
      { path: 'experience/experience-detail', component: ExperienceDetailComponent },
      { path: 'experience/experience-detail/:experienceId', component: ExperienceDetailComponent, resolve: { experienceDetails: ExperienceResolver } },
      { path: 'language/language-overview', component: LanguageOverviewComponent },
      { path: 'language/language-detail', component: LanguageDetailComponent },
      { path: 'language/language-detail/:languageId', component: LanguageDetailComponent, resolve: { languageDetails: LanguageResolver } },
      { path: 'skill/skill-overview', component: SkillOverviewComponent },
      { path: 'skill/skill-detail', component: SkillDetailComponent },
      { path: 'skill/skill-detail/:skillId', component: SkillDetailComponent, resolve: { skillDetails: SkillResolver } },
      { path: 'product/product-overview', component: ProductOverviewComponent },
      { path: 'product/product-detail', component: ProductDetailComponent },
      { path: 'product/product-detail/:productId', component: ProductDetailComponent, resolve: { productDetails: ProductResolver } }

      // { path: 'file', component: FileManagementComponent },
  ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
