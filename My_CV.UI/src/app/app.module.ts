import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './common/auth.guard';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from '../core';
import { SharedModule } from './shared/shared.module';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AgGridModule } from 'ag-grid-angular';

import { FileManagementComponent } from './pages/file-management/file-management.component';
import { LoginComponent } from './pages/login/login.component';
import { ThemeComponent } from './theme/theme.component';
import { HeaderComponent } from './theme/header/header.component';
import { FooterComponent } from './theme/footer/footer.component';
import { LeftAsideComponent } from './theme/left-aside/left-aside.component';
import { MenuComponent } from './theme/left-aside/menu/menu.component';
import { AccountComponent } from './pages/account/account.component';
import { AccountDetailYComponent } from './pages/account/account-detail-y/account-detail-y.component';
import { AccountOverviewComponent } from './pages/account/account-overview/account-overview.component';
import { InformationComponent } from './pages/information/information.component';
import { ExperienceComponent } from './pages/experience/experience.component';
import { ExperienceDetailComponent } from './pages/experience/experience-detail/experience-detail.component';
import { ExperienceOverviewComponent } from './pages/experience/experience-overview/experience-overview.component';
import { LanguageComponent } from './pages/language/language.component';
import { LanguageDetailComponent } from './pages/language/language-detail/language-detail.component';
import { LanguageOverviewComponent } from './pages/language/language-overview/language-overview.component';
import { SkillComponent } from './pages/skill/skill.component';
import { SkillDetailComponent } from './pages/skill/skill-detail/skill-detail.component';
import { SkillOverviewComponent } from './pages/skill/skill-overview/skill-overview.component'
import { ProductDetailComponent } from './pages/product/product-detail/product-detail.component';
import { ProductOverviewComponent } from './pages/product/product-overview/product-overview.component';
import { ProductComponent } from './pages/product/product.component';

import { LoginService } from './pages/login/@services/login.service';
import { FileService } from './pages/file-management/@service/file.service';
import { ExperienceService } from './pages/experience/@service/experience.service';
import { LanguageService } from './pages/language/@service/language.service';
import { SkillService } from './pages/skill/@service/skill.service';
import { ProductService } from './pages/product/@service/product.service';
import { InformationServiceService } from './pages/information/@service/information-service.service';

import { ExperienceResolver } from '../app/pages/experience/@service/experience.resolver'
import { LanguageResolver } from '../app/pages/language/@service/language.resolver';
import { SkillResolver } from '../app/pages/skill/@service/skill.resolver';
import { ProductResolver } from '../app/pages/product/@service/product.resolver';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InformationComponent,
    ThemeComponent,
    HeaderComponent,
    FooterComponent,
    LeftAsideComponent,
    MenuComponent,
    AccountComponent,
    AccountDetailYComponent,
    AccountOverviewComponent,
    FileManagementComponent,
    ExperienceComponent,
    ExperienceDetailComponent,
    ExperienceOverviewComponent,
    LanguageComponent,
    LanguageDetailComponent,
    LanguageOverviewComponent,
    SkillComponent,
    SkillDetailComponent,
    SkillOverviewComponent,
    ProductComponent,
    ProductDetailComponent,
    ProductOverviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    AppRoutingModule,
    CoreModule.forRoot(),
    SharedModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    AuthGuard,
    LoginService,
    FileService,
    InformationServiceService,
    ExperienceService,
    LanguageService,
    SkillService,
    ProductService,
    ExperienceResolver,
    LanguageResolver,
    SkillResolver,
    ProductResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
