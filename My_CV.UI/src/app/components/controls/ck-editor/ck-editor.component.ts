import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ck-editor',
  templateUrl: './ck-editor.component.html',
  styleUrls: ['./ck-editor.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CkEditorComponent),
      multi: true
    }
  ]
})
export class CkEditorComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('editor') editor: any;
  instance: any;
  private isDisable = false;
  private _value;

  @Output() valueChange = new EventEmitter();
  get value(): any {
    return this._value;
  }
  @Input()
  set value(v) {
    if (v !== this._value) {
      this._value = v;
      if (this.instance) {
        this.instance.setData(this._value);
      }
      this.onChange(v);
    }
  }


  @Input()
  set disable(v) {
    if (v) {
      this.isDisable = true;
    }
  }

  onChange: any = () => { };
  onTouched: any = () => { };
  private editorConfig = (config) => {
    config.toolbarGroups = [
      { name: 'clipboard', groups: ['clipboard', 'undo'] },
      { name: 'links' },
      { name: 'insert' },
      { name: 'forms' },
      { name: 'tools' },
      { name: 'document', groups: ['mode', 'document', 'doctools'] },
      { name: 'others' },
      '/',
      { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
      { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
      { name: 'styles' },
      { name: 'colors' }
    ];
    config.removeButtons = 'Underline,Subscript,Superscript,Source,Templates';
    config.removeButtons = 'Copy,Paste,PasteText,PasteFromWord,SpellChecker';
    config.format_tags = 'p;h1;h2;h3;pre';
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.startupFocus = 'end';
    config.extraPlugins = 'html5video,html5audio';
    config.removeDialogTabs = 'image:advanced;image:Link;link:advanced;link:upload';
    config.filebrowserImageUploadUrl = 'http://localhost:5000/api/values/UploadImage';
  }

  constructor() {
  }

  ngOnInit() {
    this.ckeditorInit();
  }

  ckeditorInit() {
    if (typeof CKEDITOR === 'undefined') {
      console.warn('CKEditor 4.x is missing (http://ckeditor.com/)');
    } else {
      if (this.instance || !this.documentContains(this.editor.nativeElement)) {
        return;
      }
      // CKEditor replace textarea
      this.instance = CKEDITOR.replace(this.editor.nativeElement, this.editorConfig);
      this.instance.setData(this._value);
      this.instance.on('change', () => {
        this._value = this.instance.getData();
        this.valueChange.emit(this._value);
      });
    }
  }

  private documentContains(node: Node) {
    return document.contains ? document.contains(node) : document.body.contains(node);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.readonly && this.instance) {
      this.instance.setReadOnly(changes.readonly.currentValue);
    }
  }

  ngOnDestroy(): void {
    if (this.instance) {
      setTimeout(() => {
        this.instance.removeAllListeners();
        CKEDITOR.instances[this.instance.name].destroy();
        this.instance.destroy();
        this.instance = null;
      });
    }
  }

  writeValue(value) {
    if (value) {
      this.valueChange.emit(this._value);
    }
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

}
