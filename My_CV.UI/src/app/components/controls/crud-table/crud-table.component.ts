import { Component, OnInit, Output, Input, EventEmitter, ElementRef } from '@angular/core';
import { Common } from 'src/app/common';
import { SharingService } from 'src/app/shared/services/sharing.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'crud-table',
  templateUrl: './crud-table.component.html',
  styleUrls: ['./crud-table.component.css']
})
export class CrudTableComponent implements OnInit {
  public $jsGrid;
  public _api;
  private _dataSource = [];
  public _isLoading = false;
  private _visibleCol = null;
  private isOptionInit = false;
  @Input() printField: any = null;
  @Input() printTitle = '';

  /**
   * @description Default loading spinner template
   * @type {string}
   * @memberof LoadingSpinnerComponent
   */
  public template = `
  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`;
  private isHiddenColumnBox = false;

  @Input()
  set isLoading(val) {
    this._isLoading = val || false;
  }
  get isLoading() {
    return this._isLoading;
  }
  @Output() dataSourceChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  set dataSource(val) {
    // tslint:disable-next-line:triple-equals
    if (val != this.dataSource) { // check if data change => reload grid
      if (val && val.length > 0) {
        this._dataSource = val;
      } else {
        this._dataSource = [];
      }
      if (!this.isOptionInit) {
        const __watching = setInterval(() => {
          if (this.isOptionInit) {
            this._options.data = this._dataSource;
            this.dataSourceChange.emit(this._dataSource);
            if (this.gridIsEmpty()) {
              this.$jsGrid.jsGrid(this._options);
            } else {
              this.reloadGrid();
            }

            clearInterval(__watching);
          }
        }, 300, 10);
      } else {
        this._options.data = this._dataSource;
        this.dataSourceChange.emit(this._dataSource);
        if (this.gridIsEmpty()) {
          this.$jsGrid.jsGrid(this._options);
        } else {
          this.reloadGrid();
        }
      }
    }
  }

  public showExport = false;

  @Output() visibleColChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  set visibleCol(val) {

  }
  get dataSource() {
    return this._dataSource;
  }
  public _options: any = {
    width: '100%',
    heading: true,
    height: '500px',
    autoload: true,
    filtering: true,
    inserting: true,
    editing: true,
    sorting: true,
    pageLoading : true,
    paging: true,
    pageSize: 20,
    pagerFormat: `Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount}`,
    pagePrevText: 'Prev',
    pageNextText: 'Next',
    pageFirstText: 'First',
    pageLastText: 'Last',
    confirmDeleting: false,
    pageButtonCount: 5,
    enableResize: false,
    enableSorable: false,
    enableEditableColumn: false,
    data: this._dataSource || [],
    controller: {},
    onRefreshed: (args) => {
      if (this._options.enableResize) {
        this.jsGridResizable(args);
      }
      if (this._options.enableSorable) {
        this.jsGridSorable(args);
      }
    },
    rowClick: function (args) {
      if (this.editing) {
        this.editItem($(args.event.target).closest('tr'));
      }
      $(args.event.currentTarget.previousElementSibling).find('input').on('keydown', function (e) {
        if (e.which === 13) {
          $('.crud-table').jsGrid('updateItem');
          return false;
        }
      });
    },
    onItemEditing: args => {
      $(document).keyup((e) => {
        if (e.key === 'Escape') {
          this.$jsGrid.jsGrid('cancelEdit');
        }
      });
    },
    onDataLoaded: (args) => {
      let $result = args.grid.fields;
      $result.forEach(item => {
        $(item.insertControl).on('keydown', (e) => {
          if (e.which === 13) {
            this.$jsGrid.jsGrid('insertItem');
            this.$jsGrid.jsGrid().insertFailed = true; // clear insert Field after insert success
            return false;
          }
        });
      });

      args.grid._body.find('tr:last').children().css('background', '#C7C7C7');
      args.grid._body.find('tr:last').css({
        'font-weight': 'bold',
        'color': 'black'
      });
    }
  };
  @Input()
  set options(config) {
    this._options = _.assign({}, this._options, config);
    this.showExport = this._options.excel ? true : false;
    this.isOptionInit = true;
    if (this._options.enableEditableColumn) {
      this.jsGridAddEditableColumn();
    }
  }
  constructor(private elemRef: ElementRef, public sharing: SharingService, public router: Router) { }

  ngOnInit() {
    this.$jsGrid = $(this.elemRef.nativeElement).find('.crud-table');
  }

  private reloadGrid() {
    this.$jsGrid = $(this.elemRef.nativeElement).find('.crud-table');
    this.$jsGrid.jsGrid(this._options);
  }

  private gridIsEmpty() {
    let $elem = $(this.elemRef.nativeElement).find('.crud-table');
    let result = $elem.html() === '';
    if (result) {
      this.$jsGrid = $elem;
    }
    return result;
  }

  jsGridSorable(args) {
    // sortable column
    let $headerRow = $(args.grid._headerRow).filter('.jsgrid-header-row');
    let $headerCells = $headerRow.find('th');
    let fields = args.grid.option('fields');
    let that = this;

    $.each(fields, function (index, field) {
      $headerCells.eq(index).data('JSField', field);
    });

    $headerRow.sortable({
      axis: 'x',
      placeholder: 'header-cell-placeholder',
      start: function (e, ui) {
        ui.placeholder.width(ui.helper.width());
      },
      update: function (e, ui) {
        let fields = $.map($headerRow.find('th'), function (cell) {
          return $(cell).data('JSField');
        });

        that.$jsGrid.jsGrid('option', 'fields', fields);
      }
    });
  }

  jsGridResizable(args) {
    // resize column
    // sync column width on page load
    $.each(args.grid._headerGrid[0].rows[0].cells, function (i, obj) {
      $(args.grid._bodyGrid[0].rows[0].cells[i]).css('width', $(obj).css('width'));
    });

    // sync column width on column resize
    $('table').colResizable({
      onResize: function () {
        $.each(args.grid._headerGrid[0].rows[0].cells, function (i, obj) {
          $(args.grid._bodyGrid[0].rows[0].cells[i]).css('width', $(obj).css('width'));
        });
      }
    });
  }

  jsGridAddEditableColumn() {
    let visibleColumn = {
      width: 20,
      align: 'center',
      name: 'jsVisibleColumn',
      sorting: false,
      headerTemplate: () => {
        let boxCheckboxField = $(`<div id='visible-box' style='width: 200px; border-radius:10px; height: 135px;
        background-color: white; border: 1px solid grey; margin-top: 5px; width: 210px; overflow-y: auto;
        position: absolute; margin-left: -173px;'>`).hide();

        // add column filter area
        let filterBox = $(`<input type='input' id='column-filter' style='margin-top: 5px;-webkit-border-radius: 50px;
        -moz-border-radius: 50px;outline: none; border-radius: 50px;' placeholder="Column Name"/>`).on('keyup', value => {
            if (value.which == 27) {
              $('#visible-icon').trigger('click');
              return;
            }
            let filter = $(value.target).val();
            let allCoumn = $('.box');
            allCoumn.parent().hide();
            $.each(allCoumn, function (idx, item) {
              let itemvalue = $(item).parent().text().trim().toLowerCase()
              if (itemvalue.includes(filter.toLowerCase())) {
                $(item).parent().show();
              }
            });
          });
        boxCheckboxField.append(filterBox);

        let fields = $(this.$jsGrid).data('JSGrid').fields;

        // generate checkbox column in grid
        $.each(fields, function (idx, value) {
          if (value.title) {
            let checkbox = `<label style='display: block;'><input type='checkbox' class='box' value='${value.name}'> ${value.title}</label>`;
            if (value.visible) {
              // $(checkbox).prop('checked', true);
              checkbox = `<label style='display: block;'><input type='checkbox' class='box' value='${value.name}' checked> ${value.title}</label>`;
            } else {
              $(checkbox).prop('checked', false);
            }
            boxCheckboxField.append(checkbox);
          }
        });
        let visibleIcon = $(`<i id='visible-icon' style='color:green;' class='fa fa-eye'>`).on('click', () => {
          if ($('#visible-box').css('display') === 'none' || !this.isHiddenColumnBox) {
            $('#visible-box').show();
            this.isHiddenColumnBox = true;
          } else {
            $('#visible-box').hide();
            this.isHiddenColumnBox = false;
          }
        });
        let divWrapper = $(`<div id='visible-column'></div>`).append(visibleIcon).append(boxCheckboxField);
        setTimeout(() => {
          // tslint:disable-next-line:no-shadowed-variable
          let that = this;
          $('.box').click(function () {
            let fieldCheck = $('.crud-table').data('JSGrid').fields;
            let colValue = $(this).val();
            let check = $(this);
            $.each(fieldCheck, function (idx, value) {
              if (value.name === colValue) {
                value.visible = check.prop('checked');
              }
            });
            $('.crud-table').jsGrid('option', 'fields', fieldCheck);
            that.isHiddenColumnBox = true;
            $('#visible-icon').trigger('click');
          });
        }, 1000);
        return divWrapper;
      }
    };
    if (this._options.fields && this._options.fields.length > 0) {
      let existsVisivleColumn = this._options.fields.find(item => item.name == 'jsVisibleColumn');
      if (!existsVisivleColumn) {
        this._options.fields.push(visibleColumn);
        this.reloadGrid();
      }
    }

  }

  printData(type) {
    let printApi = new Promise((printResolve) => {
      printResolve({ data: this.dataSource, props: this.printField, title: this.printTitle });
    });
    this.sharing.setPrintPromise(printApi);
    this.router.navigate(['print']);
  }
  exportExcel(e) {
    let $fakeTitleRow = this.$jsGrid.find('table:first').find('tr:first').clone();
    let $fakeBodyTable = this.$jsGrid.find('table:last').clone();
    $fakeBodyTable.prepend($fakeTitleRow);
    Common.createExcelFile(window, $fakeBodyTable[0], 'Sheet1', this._options.excel.fileName);
  }

  public createFakeGrid() {
    $(this.elemRef.nativeElement).find('.fake-grid').empty();
    let $fakeTitleRow = this.$jsGrid.find('table:first').find('tr:first').clone();
    let $fakeBodyTable = this.$jsGrid.find('table:last').clone();
    $fakeBodyTable.prepend($fakeTitleRow);
    $fakeBodyTable.addClass('table').addClass('table-bordered').removeClass('jsgrid-table');
    $fakeBodyTable.find('tr,td,th').removeAttr('class');
    $(this.elemRef.nativeElement).find('.fake-grid').append($fakeBodyTable);
    this.$jsGrid.addClass('hidden-print');
  }

  reloadFakeGrid() {
    this.createFakeGrid();
  }

}

