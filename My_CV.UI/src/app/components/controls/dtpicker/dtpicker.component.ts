import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ElementRef, OnChanges, forwardRef, HostListener } from '@angular/core';
import { Common } from '../../../common/index';
import * as moment from 'moment';

@Component({
    selector: 'p-dtpicker',
    templateUrl: './dtpicker.component.html',
    styleUrls: ['./dtpicker.component.css']
})
export class DtpickerComponent implements OnInit, AfterViewInit, OnChanges {

    public DTPICKERPROP = 'DateTimePicker';
    public $dtpicker;

    @Input() inline;

    // Two-way data binding [date]
    public _date;
    @Output() dateChange: EventEmitter<any> = new EventEmitter<any>();
    @Input()
    set date(val) {
        if (val !== undefined) {
            if (this.$dtpicker !== undefined && val !== this.date) {
                this._date = val;
                this.updateValue();
                this.onChange(val);
            } else {
                this._date = val;
            }

            this.dateChange.emit(val);

            if (this.$dtpicker === undefined) {
                this.bindDtPicker();
            }
        }
    }
    get date() {
        return this._date;
    }

    // Two-way data binding [disabled]
    public _disabled;
    @Output() disabledChange: EventEmitter<any> = new EventEmitter<any>();
    @Input()
    set disabled(val) {
        this._disabled = val;
        this.disabledChange.emit(val);
    }
    get disabled() {
        return this._disabled;
    }

    @Input() format;
    @Output() change = new EventEmitter();
    @Output() dateRender = new EventEmitter();

    onChange: any = () => { };

    constructor(
        public elemRef: ElementRef
    ) { }

    bindDtPicker() {
        let $elem = $(this.elemRef.nativeElement);

        if (this.inline === true) {
            $elem.find('div:first>input').empty();
        }

        this.$dtpicker = $elem.find('div:first>input');

        this.$dtpicker.datepicker({
            format: this.format || 'dd.mm.yyyy',
            orientation: 'bottom auto'
        });
        this.$dtpicker.datepicker('setDate', this.date);
        this.$dtpicker.datepicker().on('changeDate', (date) => {
            this.change.emit(moment(date.date).format(this.format || 'dd.mm.yyyy'));
        });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }

    updateValue() {
        this.$dtpicker.datepicker('setDate', this.date);
    }

    ngOnChanges(changes: any) {
        if (this.$dtpicker !== undefined) {
            this.$dtpicker.find('input').attr('disabled', this.disabled);
        }
    }
}
