import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Select2Component } from './select2.component';
import { FormsModule } from '@angular/forms';
import { ElementRef } from '@angular/core';

describe('Select2Component', () => {
  let ElementRefStub: Partial<ElementRef>;

  let component: Select2Component;
  let fixture: ComponentFixture<Select2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        FormsModule
      ],
      declarations: [
        Select2Component
      ],
      providers: [
        { provide: ElementRef, useValue: ElementRefStub }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(Select2Component);
    component = fixture.componentInstance;

    ElementRefStub = TestBed.get(ElementRef);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
