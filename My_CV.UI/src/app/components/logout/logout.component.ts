import { Cache } from './../../common/cache';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private _router: Router, private _authService: AuthService
  ) { }

  ngOnInit() {
    this._authService.clearUserInfo();
    this._router.navigate(['login']);
  }

}
