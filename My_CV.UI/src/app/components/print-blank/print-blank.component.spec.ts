import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintBlankComponent } from './print-blank.component';

describe('PrintBlankComponent', () => {
  let component: PrintBlankComponent;
  let fixture: ComponentFixture<PrintBlankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintBlankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBlankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
