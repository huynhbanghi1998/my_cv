import { Component, OnInit, AfterViewInit, AfterContentChecked, AfterContentInit } from '@angular/core';
import { SharingService } from 'src/app/shared/services/sharing.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-print-blank',
  templateUrl: './print-blank.component.html',
  styleUrls: ['./print-blank.component.css']
})
export class PrintBlankComponent implements OnInit, AfterContentInit {
  public data;
  public props;
  public title;

  public page;
  constructor(
    public sharing: SharingService,
    public router: Router,
    public location: Location) { }

  ngOnInit() {

  }

  ngAfterContentInit() {

    this.removeAlert();

    if (this.sharing.getPrintPromise() === undefined) {
      this.router.navigate(['dsb/home']);
      console.log('printing promise was null');
    } else {
      this.sharing.getPrintPromise().then((res) => {

        res.props = res.props.filter(
          prop => prop['name'] !== '' && prop['field'] !== '');

        res.props.forEach(prop => {
          if (prop.itemTemplate) {
            res.data.forEach(d => {
              try {
                d[prop.name] = d[prop.field] = prop.itemTemplate(d[prop.field] || d[prop.name]);
              } catch (err) { }
            });
          }
        });

        this.data = res.data;
        this.props = res.props;
        this.title = res.title;
        this.page = res.page;

        setTimeout(() => {
          window.print();
          this.sharing.ngOnDestroy();
          this.location.back();
        }, 1000);

      });
    }
  }

  classAppend(prop) {
    return this.page + '_' + prop;
  }

  removeAlert() {
    $('.alert').hide();
  }

}
