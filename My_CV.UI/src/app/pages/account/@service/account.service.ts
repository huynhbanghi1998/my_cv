import { AccountUrl } from './../../../shared/urls/account.url';
import { Injectable } from '@angular/core';
import { HttpService } from 'src/core';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private _httpService: HttpService) {
  }
  UpdatePassword(passInfo) {
    return this._httpService.postData(AccountUrl.UpdatePassword, passInfo).map(res => res.json());
  }
}
