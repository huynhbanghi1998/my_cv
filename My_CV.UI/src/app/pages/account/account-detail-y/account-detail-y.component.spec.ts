import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailYComponent } from './account-detail-y.component';

describe('AccountDetailYComponent', () => {
  let component: AccountDetailYComponent;
  let fixture: ComponentFixture<AccountDetailYComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailYComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailYComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
