import { Component, OnInit } from '@angular/core';
import { AccountService } from './@service/account.service';
import { AuthGuard } from 'src/app/common/auth.guard';
import { ResponseStatus } from 'src/app/shared/enums/response-status.enum';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public password = {
    newPass: '',
    confirmPass: '',
    oldPass: ''
  };

  constructor(private _accountService: AccountService) { }

  ngOnInit() {
  }

  changePassword() {
    this._accountService.UpdatePassword(this.password).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Đổi Pass Thành Công',
          type: 'success',
          icon: 'success',
        }).catch(swal.noop);
      } else {
        swal({
          title: 'vui lòng thử lại',
          type: 'warning',
          text: res.data,
        }).catch(swal.noop);
      }
    });
  }
}
