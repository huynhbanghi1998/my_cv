export class ExperienceModel {
    public id: string;
    public dateFrom: Date;
    public dateTo: Date;
    public company: string;
    public tittle: string;
    public description: string;
    constructor(obj) {
        this.id = obj.id || '';
        this.dateFrom = obj.dateFrom || '';
        this.dateTo = obj.dateTo || '';
        this.company = obj.company || '';
        this.tittle = obj.tittle || '';
        this.description = obj.description || '';
    }
}