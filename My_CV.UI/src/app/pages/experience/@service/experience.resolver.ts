import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "../../../../../node_modules/@angular/router";
import { ExperienceService } from "./experience.service";
import { Observable } from "../../../../../node_modules/rxjs";
import { Injectable } from "../../../../../node_modules/@angular/core";

@Injectable()
export class ExperienceResolver implements Resolve<any> {

    constructor(
        public _experienceService: ExperienceService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const experienceId = route.params['experienceId'];
        return this._experienceService.getExperienceById(experienceId);
    }
}