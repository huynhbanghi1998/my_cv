import { Injectable } from '@angular/core';
import { HttpService } from '../../../../core';
import { ExperienceUrl } from '../../../shared/urls/experience.url';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  constructor(private _httpService: HttpService) { }

  getExperienceList() {
    return this._httpService.get(ExperienceUrl.GetExperienceList).map(res => res.json());
  }

  getExperienceById(Id) {
    return this._httpService.getById(ExperienceUrl.GetExperienceById, Id).map(res => res.json());
  }

  updateExperience(Experience) {
    return this._httpService.uploadFile(ExperienceUrl.UpdateExperience, Experience).map(res => res.json());
  }

  insertExperience(Experience) {
    return this._httpService.uploadFile(ExperienceUrl.InsertExperience, Experience).map(res => res.json());
  }
  deleteExperience(id) {
    return this._httpService.getById(ExperienceUrl.DeleteExperience, id).map(res => res.json());
  }
}
