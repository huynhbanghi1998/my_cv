import { Component, OnInit } from '@angular/core';
import { PageMode, Common } from '../../../common';
import { ExperienceModel } from '../@model/experience.model';
import { ApiConfig } from '../../../../config/api.config';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { ExperienceService } from '../@service/experience.service';
import { ResponseStatus } from '../../../shared/enums/response-status.enum';

@Component({
  selector: 'app-experience-detail',
  templateUrl: './experience-detail.component.html',
  styleUrls: ['./experience-detail.component.scss']
})
export class ExperienceDetailComponent implements OnInit {
  public pageMode = PageMode.ADD;
  public experience = new ExperienceModel({
    id: '',
    dateFrom: '',
    dateTo: '',
    company: '',
    tittle: '',
    description: ''
  });

  constructor(public activatedRoute: ActivatedRoute, private _experienceService: ExperienceService, private _route: Router) { }

  ngOnInit() {
    const paramSubs = this.activatedRoute.params
      .subscribe(params => {
        this.experience.id = params['experienceId'];
        if (this.experience.id) {
          if (this.activatedRoute.snapshot.data['experienceDetails']) {
            let experienceData = this.activatedRoute.snapshot.data['experienceDetails'];
            if (experienceData.status == 'success') {
              this.experience = new ExperienceModel(experienceData.data);
              this.pageMode = PageMode.EDIT;
            }
          }
        }
      });
  }

  saveExperience() {
    let formData = new FormData();
    formData = Common.parseFormFileFromJson(this.experience);
    if (this.pageMode == PageMode.EDIT) {
      this._experienceService.updateExperience(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Update Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/experience/experience-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Update lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    } else if (this.pageMode == PageMode.ADD) {
      this._experienceService.insertExperience(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Thêm Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/experience/experience-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Thêm lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    }
  }
  deleteExperience() {
    this._experienceService.deleteExperience(this.experience.id).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Xóa Thành Công',
          type: 'success',
          icon: 'success',
        }).then(() => {
          this._route.navigate([`dsb/experience/experience-overview`]);
        }).catch(swal.noop);
      } else {
        swal({
          title: 'Update lỗi vui lòng thử lại',
          type: 'error',
        }).catch(swal.noop);
      }
    });
  }

}
