import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { ExperienceService } from '../@service/experience.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-experience-overview',
  templateUrl: './experience-overview.component.html',
  styleUrls: ['./experience-overview.component.scss']
})
export class ExperienceOverviewComponent implements OnInit {
  public columnDefs = [
    { headerName: 'Company name', field: 'company' },
    { headerName: 'Date from', field: 'dateFrom' },
    { headerName: 'Date to', field: 'dateTo' },
    { headerName: 'Position', field: 'tittle' },
    { headerName: 'Description', field: 'description' },
    {
      headerName: 'Edit',
      cellRenderer: (params) => {
        let eDiv = document.createElement('div');
        eDiv.innerHTML = `<span class="my-css-class">
        <button class="btn-edit btn m-btn--pill btn-brand m-btn--bolder m-btn--uppercase">Edit</button>
        </span>`;
        let eEditButton = eDiv.querySelectorAll('.btn-edit')[0];
        eEditButton.addEventListener('click', () => {
          this.router.navigate([`dsb/experience/experience-detail/${params.data.id}`]);
        });
        return eDiv;
      },
      autoHeight: true
    }
  ];

  public experienceData = null;
  public rowData: any;
  constructor(private componentFactoryResolver: ComponentFactoryResolver,
    private _experienceService: ExperienceService, private router: Router) { }

  ngOnInit() {
    this.getExperienceList();
  }

  getExperienceList() {
    this._experienceService.getExperienceList().subscribe(res => {
      if (res && res.status == 'success') {
        this.rowData = res.data;
      }
    });
  }

  addExperience() {
    this.router.navigate([`dsb/experience/experience-detail`]);
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }

}
