import { Injectable } from '@angular/core';
import { HttpService } from 'src/core';
import { ValuesUrl } from 'src/app/shared/urls/values.url';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private _httpService: HttpService) { }
  getFileList() {
    return this._httpService.get(ValuesUrl.GetFileList).map(res => res.json());
  }

  deleteFileItem(fileName) {
    return this._httpService.getWithData(ValuesUrl.DeleteFileItem, {fileName: fileName}).map(res => res.json());
  }

}
