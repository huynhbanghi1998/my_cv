import { Component, OnInit } from '@angular/core';
import { FileService } from './@service/file.service';
import { ApiConfig } from 'src/config/api.config';

@Component({
  selector: 'app-file-management',
  templateUrl: './file-management.component.html',
  styleUrls: ['./file-management.component.scss']
})
export class FileManagementComponent implements OnInit {
  public fileList = [];
  constructor(private _fileService: FileService) { }

  ngOnInit() {
    this.getFileUploaded();
  }
  getFileUploaded() {
    this._fileService.getFileList().subscribe(res => {
      if (res) {
        this.fileList = res.map(file => {
          return file = `${ApiConfig.host}/image/${file}`;
        });
      }
    });

  }
  deleteImage(imageName) {
    this._fileService.deleteFileItem(imageName.replace(`${ApiConfig.host}/image/`, '')).subscribe(res => {
      if (res) {
        this.fileList = res.map(file => {
          return file = `${ApiConfig.host}/image/${file}`;
        });
      }
    });
  }
  saveUpload() {
  }

}
