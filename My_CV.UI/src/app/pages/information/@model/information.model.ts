import { ApiConfig } from 'src/config/api.config';

export class InformationModel {
    public id: string;
    public name: string;
    public birthday: string;
    public skype: string;
    public marital: string;
    public phone: string;
    public nationality: string;
    public email: string;
    public aboutMe: string;
    public avatarURL: string;
    public cover1URL: string;
    public cover2URL: string;
    constructor(obj) {
        this.id = obj.id || '';
        this.name = obj.name || '';
        this.birthday = obj.birthday || '';
        this.skype = obj.skype || '';
        this.marital = obj.marital || '';
        this.phone = obj.phone || '';
        this.nationality = obj.nationality || '';
        this.email = obj.email || '';
        this.aboutMe = obj.aboutMe || '';
        this.avatarURL = obj.avatarURL ? ApiConfig.host + '/image/' + obj.avatarURL : '';
        this.cover1URL = obj.cover1URL ? ApiConfig.host + '/image/' + obj.cover1URL : '';
        this.cover2URL = obj.cover2URL ? ApiConfig.host + '/image/' + obj.cover2URL : '';
    }
}