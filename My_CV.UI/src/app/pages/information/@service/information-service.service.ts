import { ContentDto } from './../../../shared/@models/content.model';
import { HttpService } from './../../../../core/services/http.service';
import { Injectable } from '@angular/core';
import { InformationUrl } from 'src/app/shared/urls/information.url';

@Injectable({
  providedIn: 'root'
})
export class InformationServiceService {

  constructor(private _httpService: HttpService) { }

  getInformation() {
    return this._httpService.get(InformationUrl.GetInformationList).map(res => res.json());
  }

  updateInformation(newInformation) {
    return this._httpService.postData(InformationUrl.UpdateInformation, newInformation).map(res => res.json());
  }
}
