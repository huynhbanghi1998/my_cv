import { Component, OnInit } from '@angular/core';
import { InformationServiceService } from './@service/information-service.service';
import { ResponseStatus } from './../../shared/enums/response-status.enum';
import { ApiConfig } from 'src/config/api.config';
import { Common } from 'src/app/common';
const swal = require('sweetalert2');

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {
  public isChangeImage = false;
  public Information_new = {
    name: '',
    birthday: '',
    skype: '',
    marital: '',
    phone: '',
    nationality: '',
    email: '',
    aboutMe: '',
    avatar: '',
    cover1: '',
    cover2: ''
  };

  public editorOptions = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: true,
    heightMin: 300,
    heightMax: 300,
    requestWithCredentials: true,
    imageUploadURL: ApiConfig.url() + '/values/upload_image_to_dictionary',
    imageManagerLoadURL: ApiConfig.url() + '/values/get-dictionary-image',
    imageManagerDeleteURL: ApiConfig.url() + '/values/delete-dictionary-image',
    videoUpload: false,
    videoAllowedProviders: ['youtube', 'vimeo'],
    imageUploadParam: 'file'
  };

  constructor(private _informationServiceService: InformationServiceService) { }

  ngOnInit() {
    this.initImageReview();
    this.getInformation();
  }

  getInformation() {
    this._informationServiceService.getInformation().map(res => res.data).subscribe(res => {
      res.forEach(item => {
        this.Information_new.name = item.name;
        this.Information_new.birthday = item.birthday;
        this.Information_new.skype = item.skype;
        this.Information_new.marital = item.marital;
        this.Information_new.phone = item.phone;
        this.Information_new.nationality = item.nationality;
        this.Information_new.email = item.email;
        this.Information_new.aboutMe = item.aboutMe;
        this.Information_new.avatar = ApiConfig.host + '/image/' + item.avatarUrl;
        this.Information_new.cover1 = ApiConfig.host + '/image/' + item.coverImage1Url;
        this.Information_new.cover2 = ApiConfig.host + '/image/' + item.coverImage2Url;
      })
    })
  }

  saveInformation() {
    let formData = new FormData();
    formData = Common.parseFormFileFromJson(this.Information_new);
    if (this.isChangeImage) {
      formData.append('fileImage', $('#img_avatar')[0].files[0]);
      formData.append('fileImage', $('#img_cover1')[0].files[0]);
      formData.append('fileImage', $('#img_cover2')[0].files[0]);
    }
    this._informationServiceService.updateInformation(formData).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Update Thành Công',
          type: 'success',
          icon: 'success',
        }).catch(swal.noop);
      } else {
        swal({
          title: 'Update lỗi vui lòng thử lại',
          type: 'error',
        }).catch(swal.noop);
      }
    });
  }

  initImageReview() {
    const that = this;
    $('#img_avatar').change(function () {
      that.readURL(this, 1);
    });
    $('#img_cover1').change(function () {
      that.readURL(this, 2);
    });
    $('#img_cover2').change(function () {
      that.readURL(this, 3);
    });
    $('#btn-upload_avatar').click(() => {
      $('#img_avatar').trigger('click');
    });
    $('#btn-upload_cover1').click(() => {
      $('#img_cover1').trigger('click');
    });
    $('#btn-upload_cover2').click(() => {
      $('#img_cover2').trigger('click');
    });
  }

  readURL(input, type) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();
      switch (type) {
        case 1:
          reader.onload = (e: any) => {
            $('#avatar_image').attr('src', e.target.result);
            this.isChangeImage = true;
          };
          reader.readAsDataURL(input.files[0]);
          break;
        case 2:
          reader.onload = (e: any) => {
            $('#cover1_image').attr('src', e.target.result);
            this.isChangeImage = true;
          };
          reader.readAsDataURL(input.files[0]);
          break;
        case 3:
          reader.onload = (e: any) => {
            $('#cover2_image').attr('src', e.target.result);
            this.isChangeImage = true;
          };
          reader.readAsDataURL(input.files[0]);
          break;
      }
    }
  }
}
