export class LanguageModel {
    public id: string;
    public language: string;
    public description: string;
    public flag: string;
    constructor(obj) {
        this.id = obj.id || '';
        this.language = obj.language || '';
        this.description = obj.description || '';
        this.flag = obj.flag || '';
    }
}