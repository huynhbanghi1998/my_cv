import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "../../../../../node_modules/@angular/router";
import { LanguageService } from "./language.service";
import { Observable } from "../../../../../node_modules/rxjs";
import { Injectable } from "../../../../../node_modules/@angular/core";

@Injectable()
export class LanguageResolver implements Resolve<any> {

    constructor(
        public _languageService: LanguageService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const languageId = route.params['languageId'];
        return this._languageService.getLanguageById(languageId);
    }
}