import { Injectable } from '@angular/core';
import { HttpService } from '../../../../core';
import { LanguageUrl } from '../../../shared/urls/language.url';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private _httpService: HttpService) { }

  getLanguageList() {
    return this._httpService.get(LanguageUrl.GetLanguageList).map(res => res.json());
  }

  getLanguageById(Id) {
    return this._httpService.getById(LanguageUrl.GetLanguageById, Id).map(res => res.json());
  }

  updateLanguage(Language) {
    return this._httpService.uploadFile(LanguageUrl.UpdateLanguage, Language).map(res => res.json());
  }

  insertLanguage(Language) {
    return this._httpService.uploadFile(LanguageUrl.InsertLanguage, Language).map(res => res.json());
  }
  deleteLanguage(id) {
    return this._httpService.getById(LanguageUrl.DeleteLanguage, id).map(res => res.json());
  }
}
