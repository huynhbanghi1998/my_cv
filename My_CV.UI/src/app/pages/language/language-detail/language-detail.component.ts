import { Component, OnInit } from '@angular/core';
import { PageMode, Common } from '../../../common';
import { LanguageModel } from '../@model/language.model';
import { ApiConfig } from '../../../../config/api.config';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { ResponseStatus } from '../../../shared/enums/response-status.enum';
import { LanguageService } from '../@service/language.service';

@Component({
  selector: 'app-language-detail',
  templateUrl: './language-detail.component.html',
  styleUrls: ['./language-detail.component.scss']
})
export class LanguageDetailComponent implements OnInit {
  public pageMode = PageMode.ADD;
  public language = new LanguageModel({
    id: '',
    language: '',
    description: '',
    flag: ''
  });

  constructor(public activatedRoute: ActivatedRoute, private _languageService: LanguageService, private _route: Router) { }

  ngOnInit() {
    const paramSubs = this.activatedRoute.params
      .subscribe(params => {
        this.language.id = params['languageId'];
        if (this.language.id) {
          if (this.activatedRoute.snapshot.data['languageDetails']) {
            let languageData = this.activatedRoute.snapshot.data['languageDetails'];
            if (languageData.status == 'success') {
              this.language = new LanguageModel(languageData.data);
              this.pageMode = PageMode.EDIT;
            }
          }
        }
      });
  }

  saveLanguage() {
    let formData = new FormData();
    formData = Common.parseFormFileFromJson(this.language);
    if (this.pageMode == PageMode.EDIT) {
      this._languageService.updateLanguage(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Update Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/language/language-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Update lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    } else if (this.pageMode == PageMode.ADD) {
      this._languageService.insertLanguage(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Thêm Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/language/language-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Thêm lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    }
  }
  deleteLanguage() {
    this._languageService.deleteLanguage(this.language.id).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Xóa Thành Công',
          type: 'success',
          icon: 'success',
        }).then(() => {
          this._route.navigate([`dsb/language/language-overview`]);
        }).catch(swal.noop);
      } else {
        swal({
          title: 'Xóa lỗi vui lòng thử lại',
          type: 'error',
        }).catch(swal.noop);
      }
    });
  }

}
