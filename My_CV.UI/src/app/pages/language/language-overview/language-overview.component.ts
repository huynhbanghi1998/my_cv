import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { LanguageService } from '../@service/language.service';

@Component({
  selector: 'app-language-overview',
  templateUrl: './language-overview.component.html',
  styleUrls: ['./language-overview.component.scss']
})
export class LanguageOverviewComponent implements OnInit {
  public columnDefs = [
    { headerName: 'Language', field: 'language' },
    { headerName: 'Level', field: 'description' },
    { headerName: 'Shortname of the country', field: 'flag' },
    {
      headerName: 'Edit',
      cellRenderer: (params) => {
        let eDiv = document.createElement('div');
        eDiv.innerHTML = `<span class="my-css-class">
        <button class="btn-edit btn m-btn--pill btn-brand m-btn--bolder m-btn--uppercase">Edit</button>
        </span>`;
        let eEditButton = eDiv.querySelectorAll('.btn-edit')[0];
        eEditButton.addEventListener('click', () => {
          this.router.navigate([`dsb/language/language-detail/${params.data.id}`]);
        });
        return eDiv;
      },
      autoHeight: true
    }
  ];

  public LanguageData = null;
  public rowData: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
    private _languageService: LanguageService, private router: Router) { }

  ngOnInit() {
    this.getLanguageList();
  }

  getLanguageList() {
    this._languageService.getLanguageList().subscribe(res => {
      if (res && res.status == 'success') {
        this.rowData = res.data;
      }
    });
  }

  addLanguage() {
    this.router.navigate([`dsb/language/language-detail`]);
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }

}
