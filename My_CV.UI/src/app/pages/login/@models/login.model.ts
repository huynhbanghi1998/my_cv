export class LoginModel {
    constructor(
        public userType: string,
        public userName: string,
        public password: string) {
    }
}