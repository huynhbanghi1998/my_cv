export class UserModel {
    public access_token: string;
    public expires_in: number;
    public refresh_token: string;
    public userProfile: UserInfo;
}

export class UserInfo {
    public userTypeName: string;
    public userType: number;
    public userName: string;
    public name: string;
    public id: string;
    public webFolder: string;
    constructor(obj) {
        this.userTypeName = obj.userTypeName || '';
        this.userType = obj.userType || -1;
        this.userName = obj.userName || '';
        this.name = obj.name || '';
        this.id = obj.id || '';
        this.webFolder = obj.webFolder || '';
    }
}
