import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { HttpService } from '../../../../core';
import { LoginModel } from '../@models/login.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService {
    constructor(private _httpService: HttpService) { }

    checkUserLogin() {
        return this._httpService.get('/account/isLogin').map(res => res.json());
    }

    login(model: LoginModel) {
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Accept', 'application/json');
        const newLocal: any = { headers: headers };
        return this._httpService.post('/account/login', model, newLocal);
    }
}
