import { Component, OnInit } from '@angular/core';
import { LoginModel } from './@models/login.model';
import { Router } from '@angular/router';
import { AuthService } from '../../../core';
import { Common, NotifyType } from '../../common';
import { LoginService } from './@services/login.service';
import { Notification } from '../../common/notification';
import { UserModel } from './@models/user.model';
import 'rxjs/add/operator/finally';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})
export class LoginComponent implements OnInit {
    public isDataLoading = false;
    public loginData = new LoginModel('1', null, null);
    constructor(private router: Router, private _authService: AuthService,
        private _loginService: LoginService) { }

    ngOnInit() {
        if (this._authService.isLoggedIn()) {
            this.onLoginSuccess();
        } else {
            this.$client_init();
        }
    }

    onLoginSuccess() {
        this.router.navigate(['dsb/information']);
    }

    $client_init() {
        jQuery(document).ready(function () {

            jQuery.backstretch(`/assets/img/login/${Common.getRandomNumber(4, 6)}-min.jpg`);

            $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function () {
                $(this).removeClass('input-error');
            });

            $('.login-form').on('submit', function (e) {

                $(this).find('input[type="text"], input[type="password"], textarea').each(function () {
                    if ($(this).val() === '') {
                        e.preventDefault();
                        $(this).addClass('input-error');
                    } else {
                        $(this).removeClass('input-error');
                    }
                });

            });
        });
    }

    onLogin() {
        let isNullLoginData = (this.loginData.userName === '' || this.loginData.password === '');
        if (isNullLoginData) {
            Notification.errorNotify('USERNAME_PASS_NOT_EMPTY');
            return;
        }
        this.isDataLoading = true;
        this._loginService.login(this.loginData).finally( () => this.isDataLoading = false).map(res => res.json()).subscribe(
            (result) => {
                if (result) {
                    if (result.status === 'warning') {
                        Notification.notify(result.result, 'fa fa-close', NotifyType.WARNING, null, 'Login fail');
                    } else if (result.status === 'success') {
                        this._authService.setUser<UserModel>(result.result);
                        this.onLoginSuccess();
                    }
                }
            },
            error => {
                Notification.notify(error.json()['error_description'], 'fa fa-close', NotifyType.DANGER, null, 'Login fail');
            }
        );
    }

}
