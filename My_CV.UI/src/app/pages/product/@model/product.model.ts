import { ApiConfig } from '../../../../config/api.config'

export class ProductModel {
    public id: string;
    public imgUrl: string;
    public productType: string;
    public product_TypeID: string;
    constructor(obj) {
        this.id = obj.id || '';
        this.imgUrl = obj.imgUrl ? ApiConfig.host + '/image/' + obj.imgUrl : '';
        this.productType = obj.productType || '';
        this.product_TypeID = obj.product_TypeID || '';
    }
}