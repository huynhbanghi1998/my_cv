import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "../../../../../node_modules/@angular/router";
import { ProductService } from "./product.service";
import { Observable } from "../../../../../node_modules/rxjs";
import { Injectable } from "../../../../../node_modules/@angular/core";

@Injectable()
export class ProductResolver implements Resolve<any> {

    constructor(
        public _productService: ProductService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const productId = route.params['productId'];
        return this._productService.getProductById(productId);
    }
}