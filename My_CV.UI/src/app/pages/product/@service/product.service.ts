import { Injectable } from '@angular/core';
import { HttpService } from 'src/core';
import { ProductUrl } from 'src/app/shared/urls/product.url';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _httpService: HttpService) { }

  getProductList() {
    return this._httpService.get(ProductUrl.GetProductList).map(res => res.json());
  }

  getProductById(Id) {
    return this._httpService.getById(ProductUrl.GetProductById, Id).map(res => res.json());
  }

  updateProduct(Product) {
    return this._httpService.uploadFile(ProductUrl.UpdateProduct, Product).map(res => res.json());
  }

  insertProduct(Product) {
    return this._httpService.uploadFile(ProductUrl.InsertProduct, Product).map(res => res.json());
  }
  deleteProduct(id) {
    return this._httpService.getById(ProductUrl.DeleteProduct, id).map(res => res.json());
  }
}
