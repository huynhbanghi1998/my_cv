import { Component, OnInit } from '@angular/core';
import { PageMode, Common } from 'src/app/common';
import { ProductModel } from '../@model/product.model';
import { ApiConfig } from 'src/config/api.config';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { ProductService } from '../@service/product.service';
import { ResponseStatus } from '../../../shared/enums/response-status.enum';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  public pageMode = PageMode.ADD;
  public isChangeImage = false;
  public product = new ProductModel({
    id: '',
    imgUrl: '',
    productType: '',
    product_TypeID: ''
  });

  public editorOptions = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: true,
    heightMin: 300,
    heightMax: 300,
    requestWithCredentials: true,
    imageUploadURL: ApiConfig.url() + '/values/upload_image_to_dictionary',
    imageManagerLoadURL: ApiConfig.url() + '/values/get-dictionary-image',
    imageManagerDeleteURL: ApiConfig.url() + '/values/delete-dictionary-image',
    videoUpload: false,
    videoAllowedProviders: ['youtube', 'vimeo'],
    imageUploadParam: 'file'
  };

  constructor(public activatedRoute: ActivatedRoute, private _productService: ProductService, private _route: Router) { }

  ngOnInit() {
    this.initImageReview();
    const paramSubs = this.activatedRoute.params
      .subscribe(params => {
        this.product.id = params['productId'];
        if (this.product.id) {
          if (this.activatedRoute.snapshot.data['productDetails']) {
            let productData = this.activatedRoute.snapshot.data['productDetails'];
            if (productData.status == 'success') {
              this.product = new ProductModel(productData.data);
              this.pageMode = PageMode.EDIT;
            }
          }
        }

      });
  }

  initImageReview() {
    const that = this;
    $('#img_product').change(function () {
      // that.isChangeAvata = true;
      that.readURL(this);
    });

    $('#btn-upload-product').click(() => {
      $('#img_product').trigger('click');
    });
  }
  readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();
      reader.onload = (e: any) => {
        $('#product_image').attr('src', e.target.result);
        // this.background.background1.file = input.files[0];
        this.isChangeImage = true;
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  saveProduct() {
    let formData = new FormData();
    formData = Common.parseFormFileFromJson(this.product);
    // formData.append('fileImage', this.background.background2.file);
    if (this.isChangeImage) {
      formData.append('fileImage', $('#img_product')[0].files[0]);
    }
    if (this.pageMode == PageMode.EDIT) {
      this._productService.updateProduct(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Update Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/product/product-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Update lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    } else if (this.pageMode == PageMode.ADD) {
      this._productService.insertProduct(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Thêm Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/product/product-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Thêm lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    }
  }
  deleteProduct() {
    this._productService.deleteProduct(this.product.id).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Xóa Thành Công',
          type: 'success',
          icon: 'success',
        }).then(() => {
          this._route.navigate([`dsb/product/product-overview`]);
        }).catch(swal.noop);
      } else {
        swal({
          title: 'Xóa lỗi vui lòng thử lại',
          type: 'error',
        }).catch(swal.noop);
      }
    });
  }

}
