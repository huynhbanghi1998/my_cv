import { Component, OnInit } from '@angular/core';
import { ProductService } from '../@service/product.service';
import { Router } from '../../../../../node_modules/@angular/router';
import { ApiConfig } from 'src/config/api.config';

@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.scss']
})
export class ProductOverviewComponent implements OnInit {
  public columnDefs = [
    {
      headerName: 'Image', field: 'imgUrl', autoHeight: true,
      cellRenderer: (params) => {
        let eDiv = document.createElement('div');
        eDiv.innerHTML = `<img style='height:100px' src='${ApiConfig.host + '/image/' + params.data.imgUrl}'>`;
        return eDiv;
      }
    },
    { headerName: 'Project type', field: 'productType' },
    { headerName: 'Project type ID', field: 'product_TypeID' },
    {
      headerName: 'Edit',
      cellRenderer: (params) => {
        let eDiv = document.createElement('div');
        eDiv.innerHTML = `<span class="my-css-class">
        <button class="btn-edit btn m-btn--pill btn-brand m-btn--bolder m-btn--uppercase">Edit</button>
        </span>`;
        let eEditButton = eDiv.querySelectorAll('.btn-edit')[0];
        eEditButton.addEventListener('click', () => {
          this.router.navigate([`dsb/product/product-detail/${params.data.id}`]);
        });
        return eDiv;
      },
      autoHeight: true
    }
  ];
  public rowData: any;
  constructor(private _productService: ProductService, private router: Router) { }

  ngOnInit() {
    this.getProductList();
  }

  getProductList() {
    this._productService.getProductList().subscribe(res => {
      // tslint:disable-next-line:triple-equals
      if (res && res.status == 'success') {
        this.rowData = res.data;
      }
    });
  }
  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }
  addProduct() {
    this.router.navigate([`dsb/product/product-detail`]);
  }

}
