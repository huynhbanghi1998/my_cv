export class SkillModel {
    public id: string;
    public skillName: string;
    public proficiency: string;
    constructor(obj)
    {
        this.id = obj.id || '';
        this.skillName = obj.skillName || '';
        this.proficiency = obj.proficiency || '';
    }
}