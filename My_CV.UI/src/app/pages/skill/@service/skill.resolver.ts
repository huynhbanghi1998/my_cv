import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "../../../../../node_modules/@angular/router";
import { SkillService } from "./skill.service";
import { Observable } from "../../../../../node_modules/rxjs";
import { Injectable } from "../../../../../node_modules/@angular/core";

@Injectable()
export class SkillResolver implements Resolve<any> {

    constructor(
        public _skillService: SkillService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const skillId = route.params['skillId'];
        return this._skillService.getSkillById(skillId);
    }
}