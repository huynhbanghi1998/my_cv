import { Injectable } from '@angular/core';
import { HttpService } from '../../../../core';
import { SkillUrl } from '../../../shared/urls/skill.url'

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private _httpService: HttpService) { }

  getSkillList() {
    return this._httpService.get(SkillUrl.GetSkillList).map(res => res.json());
  }

  getSkillById(Id) {
    return this._httpService.getById(SkillUrl.GetSkillById, Id).map(res => res.json());
  }

  updateSkill(Skill) {
    return this._httpService.uploadFile(SkillUrl.UpdateSkill, Skill).map(res => res.json());
  }

  insertSkill(Skill) {
    return this._httpService.uploadFile(SkillUrl.InsertSkill, Skill).map(res => res.json());
  }
  deleteSkill(id) {
    return this._httpService.getById(SkillUrl.DeleteSkill, id).map(res => res.json());
  }
}
