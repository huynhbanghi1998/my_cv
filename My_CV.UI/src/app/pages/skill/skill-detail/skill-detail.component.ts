import { Component, OnInit } from '@angular/core';
import { PageMode, Common } from '../../../common';
import { SkillModel } from '../@model/skill.model';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { SkillService } from '../@service/skill.service';
import { ResponseStatus } from '../../../shared/enums/response-status.enum';

@Component({
  selector: 'app-skill-detail',
  templateUrl: './skill-detail.component.html',
  styleUrls: ['./skill-detail.component.scss']
})
export class SkillDetailComponent implements OnInit {
  public pageMode = PageMode.ADD;
  public skill = new SkillModel ({
    id: '',
    skillName: '',
    proficiency: ''
  });
  
  constructor(public activatedRoute: ActivatedRoute, private _skillService: SkillService, private _route: Router) { }

  ngOnInit() {
    const paramSubs = this.activatedRoute.params
      .subscribe(params => {
        this.skill.id = params['skillId'];
        if (this.skill.id) {
          if (this.activatedRoute.snapshot.data['skillDetails']) {
            let skillData = this.activatedRoute.snapshot.data['skillDetails'];
            if (skillData.status == 'success') {
              this.skill = new SkillModel(skillData.data);
              this.pageMode = PageMode.EDIT;
            }
          }
        }
      });
  }

  saveSkill() {
    let formData = new FormData();
    formData = Common.parseFormFileFromJson(this.skill);
    if (this.pageMode == PageMode.EDIT) {
      this._skillService.updateSkill(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Update Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/skill/skill-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Update lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    } else if (this.pageMode == PageMode.ADD) {
      this._skillService.insertSkill(formData).subscribe(res => {
        if (res && res.status === ResponseStatus.Success) {
          swal({
            title: 'Thêm Thành Công',
            type: 'success',
            icon: 'success',
          }).then(() => {
            this._route.navigate([`dsb/skill/skill-overview`]);
          }).catch(swal.noop);
        } else {
          swal({
            title: 'Thêm lỗi vui lòng thử lại',
            type: 'error',
          }).catch(swal.noop);
        }
      });
    }
  }

  deleteSkill() {
    this._skillService.deleteSkill(this.skill.id).subscribe(res => {
      if (res && res.status === ResponseStatus.Success) {
        swal({
          title: 'Xóa Thành Công',
          type: 'success',
          icon: 'success',
        }).then(() => {
          this._route.navigate([`dsb/skill/skill-overview`]);
        }).catch(swal.noop);
      } else {
        swal({
          title: 'Xóa lỗi vui lòng thử lại',
          type: 'error',
        }).catch(swal.noop);
      }
    });
  }

}
