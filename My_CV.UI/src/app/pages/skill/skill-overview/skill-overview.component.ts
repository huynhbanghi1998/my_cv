import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { SkillService } from '../@service/skill.service';

@Component({
  selector: 'app-skill-overview',
  templateUrl: './skill-overview.component.html',
  styleUrls: ['./skill-overview.component.scss']
})
export class SkillOverviewComponent implements OnInit {
  public columnDefs = [
    { headerName: 'Skill', field: 'skillName' },
    { headerName: 'Your Proficiency', field: 'proficiency' },
    {
      headerName: 'Edit',
      cellRenderer: (params) => {
        let eDiv = document.createElement('div');
        eDiv.innerHTML = `<span class="my-css-class">
        <button class="btn-edit btn m-btn--pill btn-brand m-btn--bolder m-btn--uppercase">Edit</button>
        </span>`;
        let eEditButton = eDiv.querySelectorAll('.btn-edit')[0];
        eEditButton.addEventListener('click', () => {
          this.router.navigate([`dsb/skill/skill-detail/${params.data.id}`]);
        });
        return eDiv;
      },
      autoHeight: true
    }
  ];

  public SkillData = null;
  public rowData: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
    private _skillService: SkillService, private router: Router) { }

  ngOnInit() {
    this.getSkillList();
  }

  getSkillList() {
    this._skillService.getSkillList().subscribe(res => {
      if (res && res.status == 'success') {
        this.rowData = res.data;
      }
    });
  }

  addSkill() {
    this.router.navigate([`dsb/skill/skill-detail`]);
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }
}
