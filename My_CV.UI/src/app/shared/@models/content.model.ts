export class ContentDto {
    public id: String;
    public title: String;
    public description: String;
    public imgUrl: String;
    public value: String;
    public creator: String;
    constructor(item) {
        this.id = item ? item.id : '';
        this.title = item ? item.title : '';
        this.description = item ? item.description : '';
        this.imgUrl = item ? item.imgUrl : '';
        this.value = item ? item.value : '';
        this.creator = item ? item.creator : '';
    }
}

