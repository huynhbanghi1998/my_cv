export class ResultDto {
    public status: boolean;
    public eStatus: EResultDtoStatus;
    public message: string;
    public data: any;
    public data1: any;
    public data2: any;
    public title: string;
}

export enum EResultDtoStatus {
    Success,
    Error,
    Exception,
    ValidationFailed,
    Information
}
