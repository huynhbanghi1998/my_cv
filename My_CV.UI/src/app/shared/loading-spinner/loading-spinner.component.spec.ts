import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingSpinnerComponent } from './loading-spinner.component';
import { LoadingSpinnerService } from './@service/loading-spinner.service';
import { MockTranslatePipe } from 'test-mock/mock-translate-pipe';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MockLoadingSpinnerService } from 'test-mock/mock-loading-spinner';

describe('LoadingSpinnerComponent', () => {
  let LoadingSpinnerServiceStub: Partial<LoadingSpinnerService>;
  let component: LoadingSpinnerComponent;
  let fixture: ComponentFixture<LoadingSpinnerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        LoadingSpinnerComponent,
        MockTranslatePipe
      ],
      providers: [
        MockLoadingSpinnerService,
        { provide: LoadingSpinnerService, useClass: MockLoadingSpinnerService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    fixture = TestBed.createComponent(LoadingSpinnerComponent);
    component = fixture.componentInstance;
    LoadingSpinnerServiceStub = TestBed.get(MockLoadingSpinnerService);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
