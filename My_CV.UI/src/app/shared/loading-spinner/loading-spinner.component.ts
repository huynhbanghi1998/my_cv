import { LoadingSpinnerService } from './@service/loading-spinner.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnDestroy, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoadingSpinnerComponent implements OnDestroy {
   /**
   * @description Default loading spinner template
   * @type {string}
   * @memberof LoadingSpinnerComponent
   */
  private _template = `
  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`;

  /**
   * @description Loading text
   * @type {string}
   * @memberof LoadingSpinnerComponent
   */
  _loadingText = '';

  /**
   * @description Defines threhold for not to diplay if time is less than 500ms
   * @type {number}
   * @memberof LoadingSpinnerComponent
   */
  _threshold = 500;

  /**
   * @description Defines z-index property of the loading text
   * @type {number}
   * @memberof LoadingSpinnerComponent
   */
  _zIndex = 9999;
/**
   * @description Sets z-index for input text
   * @memberof LoadingSpinnerComponent
   */
  @Input() public set zIndex(value: number) {
    this._zIndex = value;
  }

  /**
   * @description returns z-index for input text
   * @readonly
   * @type {number}
   * @memberof LoadingSpinnerComponent
   */
  public get zIndex(): number {
    return this._zIndex;
  }

  /**
   * @description Accepts custom template
   * @memberof LoadingSpinnerComponent
   */
  @Input()
  public set template(value: string) {
    this._template = value;
  }


  /**
   * @description Gives the current template
   * @readonly
   * @type {string}
   * @memberof LoadingSpinnerComponent
   */
  public get template(): string {
    return this._template;
  }


  /**
   * @description Accepts loading text string
   * @memberof LoadingSpinnerComponent
   */
  @Input()
  public set loadingText(value: string) {
    this._loadingText = value;
  }


  /**
   * @description Gives loading text
   * @readonly
   * @type {string}
   * @memberof LoadingSpinnerComponent
   */
  public get loadingText(): string {
    return this._loadingText;
  }


  /**
   * @description Accepts external threshold
   * @memberof LoadingSpinnerComponent
   */
  @Input()
  public set threshold(value: number) {
    this._threshold = value;
  }


  /**
   * @description 
   * @readonly
   * @type {number}
   * @memberof LoadingSpinnerComponent
   */
  public get threshold(): number {
    return this._threshold;
  }

  /**
   * Subscription
   * @type {Subscription}
   * @memberof LoadingSpinnerComponent
   */
  subscription: Subscription;

  /**
   * @description Show/hide spinner
   * @memberof LoadingSpinnerComponent
   */
  showSpinner = false;

  /**
   * Constructor
   * @param {LoadingSpinnerService} spinnerService Spinner Service
   * @memberof LoadingSpinnerComponent
   */
  constructor(
    private spinnerService: LoadingSpinnerService
  ) {
    this.createServiceSubscription();
  }
  

  /**
   * Destroy function
   * @memberof LoadingSpinnerComponent
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Create service subscription
   * @memberof LoadingSpinnerComponent
   */
  createServiceSubscription() {
    let timer: any;

    this.subscription =
      this.spinnerService.getMessage().subscribe(show => {
        if (show) {
          if (timer) {
            return;
          }
          timer = setTimeout(function () {
            timer = null;
            this.showSpinner = show;
          }.bind(this), this.threshold);
        } else {
          if (timer) {
            clearTimeout(timer);
            timer = null;
          }
          this.showSpinner = false;
        }
      });
  }

}
