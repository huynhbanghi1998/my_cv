import { Pipe, PipeTransform } from '@angular/core';
import { Common } from '../../common/common';

@Pipe({
  name: 'decimalNumeric'
})
export class DecimalNumbericPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === null || value === undefined || value === '' || isNaN(value)) {
      return '-';
    }
    return Common.numFormat(value);
  }
}
