import { Injectable, OnDestroy, isDevMode } from '@angular/core';
import { ApiConfig } from '../../../config/api.config';
import { ResultDto, EResultDtoStatus } from '../@models/result-dto.model';
import { HttpService, AuthService } from 'src/core';
const swal = require('sweetalert2');

@Injectable()
export class SharingService implements OnDestroy {
  private persistDataPrefix = '__dat_';
  private _printPromise: Promise<any>;

  constructor(private _authService: AuthService,
    private _httpService: HttpService
    ) { }

  setPrintPromise(promise) {
    this._printPromise = promise;
  }

  getPrintPromise(): Promise<any> {
    return this._printPromise;
  }
  ngOnDestroy() {
    this._printPromise = null;
  }

  getToken(): string {
    return this._authService.getToken('access_token');
  }

  getBaseUrl(): string {
    return ApiConfig.url();
  }
  getHostUrl(): string {
    return ApiConfig.host;
  }

  public alert(result: ResultDto) {
    let that = this;

    switch (result.eStatus) {
      case EResultDtoStatus.Success:
        swal({
          title: (result.title ? result.title : 'success'),
          type: 'success',
          icon: 'success',
        }).catch(swal.noop);
        break;

      // Well-known error.Show message direction to user. Exp: Validate error
      case EResultDtoStatus.Error:
        swal({
          title: (result.title ? result : 'error'),
          type: 'warning',
          text: (result.data ?
            result.data :
            'exception-general-text')
        }).catch(swal.noop);
        break;

      // Exception: Un-known errors, issues. So just alert to customer.
      case EResultDtoStatus.Exception:
        swal({
          title: (result.title ? result.title : 'error-title'),
          type: 'error',
          text: (!isDevMode() ? 'exception' : 'Exception: ' + result.data)
        }).catch(swal.noop);
        break;

      // display some information from server
      case EResultDtoStatus.Information:
        swal({
          title: (result.title ? result.title : 'information-title'),
          type: 'info',
          text: result.data
        }).catch(swal.noop);
        break;
      default:
        console.log('No subtable Result type found!');
        break;
    }
  }
}
