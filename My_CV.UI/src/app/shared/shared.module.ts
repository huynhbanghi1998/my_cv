import { DtpickerComponent } from 'src/app/components/controls/dtpicker/dtpicker.component';
import { Select2Component } from 'src/app/components/controls/select2/select2.component';
import { LogoutComponent } from 'src/app/components/logout/logout.component';
import { SharingService } from './services/sharing.service';
import { NegativeFormatDirective } from './directives/negative-format.directive';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { LoadingSpinnerService } from './loading-spinner/@service/loading-spinner.service';
import { AutofocusDirective } from './directives/auto-focus.directive';
import { DecimalNumbericDirective } from './directives/decimal-numberic.directive';
import { DecimalNumbericPipe } from './pipes/decimal-numeric.pipe';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { DisabledAllDirective } from './directives/disabled-all.directive';
import { LanguageService } from './services/language.service';
import { CrudTableComponent } from '../components/controls/crud-table/crud-table.component';
import { SafePipe } from './pipes/safe-url.pipe';
import { PrintBlankComponent } from '../components/print-blank/print-blank.component';
import { CkEditorComponent } from '../components/controls/ck-editor/ck-editor.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        DecimalNumbericDirective,
        DecimalNumbericPipe,
        OnlyNumberDirective,
        NegativeFormatDirective,
        DisabledAllDirective,
        AutofocusDirective,
        LogoutComponent,
        Select2Component,
        DtpickerComponent,
        LoadingSpinnerComponent,
        CrudTableComponent,
        SafePipe,
        PrintBlankComponent,
        CkEditorComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        DecimalNumbericDirective,
        DecimalNumbericPipe,
        OnlyNumberDirective,
        NegativeFormatDirective,
        DisabledAllDirective,
        AutofocusDirective,
        LogoutComponent,
        Select2Component,
        DtpickerComponent,
        LoadingSpinnerComponent,
        CrudTableComponent,
        SafePipe,
        CkEditorComponent
    ], entryComponents: [
    ],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers:
            [
                LanguageService, SharingService,
                LoadingSpinnerService
            ]
        };
    }
}
