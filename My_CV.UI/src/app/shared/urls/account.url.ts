import {Injectable} from '@angular/core';
@Injectable()
export class AccountUrl {
    public static get Base_URL(): string { return '/account'; }
    public static get UpdatePassword(): string { return `${this.Base_URL}/update-pass-word`; }
}
