import {Injectable} from '@angular/core';
@Injectable()
export class ExperienceUrl {
    public static get Base_URL(): string { return '/experience'; }
    public static get GetExperienceList(): string { return `${this.Base_URL}/get-experience-list`; }
    public static get GetExperienceById(): string { return `${this.Base_URL}/get-experience-by-id`; }
    public static get UpdateExperience(): string { return `${this.Base_URL}/update-experience`; }
    public static get InsertExperience(): string { return `${this.Base_URL}/insert-experience`; }
    public static get DeleteExperience(): string { return `${this.Base_URL}/delete-experience`; }

}