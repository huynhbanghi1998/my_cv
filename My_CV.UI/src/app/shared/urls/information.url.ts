import {Injectable} from '@angular/core';
@Injectable()
export class InformationUrl {
    public static get InformationUrl_URL(): string { return '/information'; }
    public static get GetInformationList(): string { return `${this.InformationUrl_URL}/get-information`; }
    public static get UpdateInformation(): string { return `${this.InformationUrl_URL}/update-information`; }
}
