import {Injectable} from '@angular/core';
@Injectable()
export class LanguageUrl {
    public static get Base_URL(): string { return '/language'; }
    public static get GetLanguageList(): string { return `${this.Base_URL}/get-language-list`; }
    public static get GetLanguageById(): string { return `${this.Base_URL}/get-language-by-id`; }
    public static get UpdateLanguage(): string { return `${this.Base_URL}/update-language`; }
    public static get InsertLanguage(): string { return `${this.Base_URL}/insert-language`; }
    public static get DeleteLanguage(): string { return `${this.Base_URL}/delete-language`; }

}