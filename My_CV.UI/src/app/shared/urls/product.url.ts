import {Injectable} from '@angular/core';
@Injectable()
export class ProductUrl {
    public static get Base_URL(): string { return '/product'; }
    public static get GetProductList(): string { return `${this.Base_URL}/get-product-list`; }
    public static get GetProductById(): string { return `${this.Base_URL}/get-product-by-id`; }
    public static get UpdateProduct(): string { return `${this.Base_URL}/update-product`; }
    public static get InsertProduct(): string { return `${this.Base_URL}/insert-product`; }
    public static get DeleteProduct(): string { return `${this.Base_URL}/delete-product`; }

}