import {Injectable} from '@angular/core';
@Injectable()
export class SkillUrl {
    public static get Base_URL(): string { return '/skill'; }
    public static get GetSkillList(): string { return `${this.Base_URL}/get-skill-list`; }
    public static get GetSkillById(): string { return `${this.Base_URL}/get-skill-by-id`; }
    public static get UpdateSkill(): string { return `${this.Base_URL}/update-skill`; }
    public static get InsertSkill(): string { return `${this.Base_URL}/insert-skill`; }
    public static get DeleteSkill(): string { return `${this.Base_URL}/delete-skill`; }

}