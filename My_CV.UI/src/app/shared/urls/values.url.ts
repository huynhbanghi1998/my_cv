import {Injectable} from '@angular/core';
@Injectable()
export class ValuesUrl {
    public static get ValuesUrl_URL(): string { return '/values'; }
    public static get GetFileList(): string { return `${this.ValuesUrl_URL}/get-file-list`; }
    public static get DeleteFileItem(): string { return `${this.ValuesUrl_URL}/delete-file-item`; }
}
