import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
declare let mMenu: any;
declare let mLayout: any;
@Component({
	selector: 'app-theme',
	templateUrl: './theme.component.html',
	encapsulation: ViewEncapsulation.None,
})
export class ThemeComponent implements OnInit, AfterViewInit {
	constructor(private _router: Router) {
		if ($('body').data('backstretch')) {
			$.backstretch('destroy');
		  }
	}
	ngOnInit() {
	}

	ngAfterViewInit() {
		mLayout.init();
		mQuickSidebar.init();
	}

}