import { Injectable } from '@angular/core';
import { UserInfo } from 'src/app/pages/login/@models/user.model';

@Injectable()

export class AuthService {

    private USER_INFO = 'user_info';
    private SINGLE_SIGN_ON_TOKEN = '';

    public setUser<T>(user: T): void {
        localStorage[this.USER_INFO] = JSON.stringify(user);
    }

    public getUser<T>(): T | any {
        if (localStorage[this.USER_INFO]) {
            return JSON.parse(localStorage[this.USER_INFO]);
        } else {
            return null;
        }
    }

    public getToken<T>(tokenProp?: string): string {
        const user = this.getUser<T>();
        // tslint:disable-next-line:curly
        if (user) return user[tokenProp] || user.token;
    }

    public getSingleSignonToken() {
        if (this.SINGLE_SIGN_ON_TOKEN) {
            return this.SINGLE_SIGN_ON_TOKEN;
        } else {
            return null;
        }
    }

    // tslint:disable-next-line:member-ordering
    public isLoggedIn() {
        if (localStorage.getItem(this.USER_INFO) && JSON.parse(localStorage.getItem(this.USER_INFO)).access_token) {
            return true;
        }
        return false;
    }

    public clearUserInfo() {
        localStorage.removeItem(this.USER_INFO);
    }

    public getUserInfo() {
        let info = this.getUser<UserInfo>().userProfile;
        return new UserInfo(JSON.parse(info));
    }

    public forceReLogin() {
        this.clearUserInfo();
        window.location.href = '/#/login';
    }

    public setSingleSignonToken<T>(token: string): void {
        // localStorage[this.SINGLE_SIGN_ON_TOKEN] = token;
        this.SINGLE_SIGN_ON_TOKEN = token;
    }

    public clearSingleSignonToken(): void {
        localStorage.removeItem(this.SINGLE_SIGN_ON_TOKEN);
    }
};
