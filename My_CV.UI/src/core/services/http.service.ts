import { AuthService } from './auth.service';
import { IHttpService } from './../interfaces/http-service.interface';
import {
  Http, RequestMethod, Request, RequestOptions, Headers, RequestOptionsArgs,
  URLSearchParams, Response
} from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiConfig } from '../../config/api.config';

// Rxjs
import { Observable } from 'rxjs/Observable';
import { Common } from 'src/app/common';
import { ResultDto } from 'src/app/shared/@models/result-dto.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService implements IHttpService {

  public _apiPath = ApiConfig.url();

  constructor(
    private http: Http,
    private _authService: AuthService
  ) { }

  /**
   * Get current token stored in localStore/..
   * @readonly
   * @private
   * @type {string}
   * @memberof HttpService
   */
  private get TOKEN(): string {
    let token = this._authService.getToken('access_token');
    if (!token) {
      token = this._authService.getSingleSignonToken();
    }
    return token;
  }

  /**
   * Set headers each requests with authorization
   * @private
   * @returns
   * The headers contains assets token
   * @memberof HttpService
   */
  private setHeaders() {
    const authHeaders = new Headers();
    authHeaders.append('Content-Type', 'application/json');
    authHeaders.append('Accept', 'application/json');
    authHeaders.append('Authorization', 'bearer ' + this.TOKEN);
    // Fix IE cache issue
    authHeaders.append('Cache-Control', 'no-cache');
    authHeaders.append('Pragma', 'no-cache');
    authHeaders.append('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT');
    // End fix IE cache
    return authHeaders;
  }

  private execute(options: any) {
    return this.http.request(new Request(options)).catch(
      error => {
        console.log(error);
        if (error.status === 401 && error.statusText === 'Unauthorized') {
          console.log('Unauthorized status - Force user re-login');
          this._authService.forceReLogin();
        } else {

          const result = new ResultDto();

          switch (error.status) {
            case 0:
              // Common.throwErrorOrBadRequest('Your back-end server might not be available!'); 
              break;

            case 401: // Unauthorized
              console.log('Unauthorized status - Force user re-login');

              result.title = 'Unauthorized';
              result.message = 'Please login.';
              this._authService.forceReLogin();
              break;
            case 403: // Forbidden
              result.title = 'Forbidden';
              result.message = 'You dont have permission to do that action. Please contact administrator.';
              break;
            case 404: // 404 Not Found
              result.title = 'Page Not Found';
              result.message = 'Page or URL you requested not found.';
              break;
            case 408: // 408 Request Timeout
              result.title = 'Request Timeout';
              result.message = 'Please try again later. Maybe check your internet is avalible.';
              break;

            case 500:
              const token_expire_time = this._authService.getToken('token_expire');
              if (token_expire_time && new Date(token_expire_time).getTime() < new Date().getTime()) {
                this._authService.forceReLogin();
              } else {
                result.title = 'Internal server error';
                result.message = 'Ops! Internal server error. Please contact IT support.';
              }
              Common.throwErrorOrBadRequest(error.json().message);
              break;

            case 501: // Not Implemented
              result.title = 'Function is not Implemented';
              result.message = 'This function is not ready yet, try again later.';
              break;
            case 502: // Bad Gateway
              result.title = 'Bad Gateway';
              result.message = 'Bad Gateway';
              break;
            case 503: // Service Unavailable
              result.title = 'Service Unavailable';
              result.message = 'Services is offline. Please try again later.';
              break;
            default:
              result.message = error.statusText;
              result.title = 'Error';
              break;
          }
          this.alertException(result);
        }
        return Observable.throw(error);
      });
  }

  create(url: string, data: any) {

    const headers = this.setHeaders();
    const options = new RequestOptions({
      method: RequestMethod.Post,
      url: this._apiPath + url,
      withCredentials: true,
      headers: headers,
      body: JSON.stringify(data)
    });

    return this.execute(options);
  }

  update(url: string, data: any) {

    const headers = this.setHeaders();
    const options = new RequestOptions({
      method: RequestMethod.Put,
      url: this._apiPath + url,
      withCredentials: true,
      headers: headers,
      body: JSON.stringify(data)
    });

    return this.execute(options);
  }

  remove(url: string, id: any, headers?: any) {

    headers = headers ? new Headers(headers) : this.setHeaders();
    const options = new RequestOptions({
      method: RequestMethod.Delete,
      url: this._apiPath + url + '/' + id,
      withCredentials: true,
      headers: headers
    });

    return this.execute(options);
  }

  getAll(url: string) {
    const headers = this.setHeaders();

    const options = new RequestOptions({
      method: RequestMethod.Get,
      url: this._apiPath + url,
      withCredentials: true,
      headers: headers
    });

    return this.execute(options);
  }

  getById(url: string, id: any) {
    const params: URLSearchParams = new URLSearchParams();
    params.set('id', id);
    const headers = this.setHeaders();
    const options = new RequestOptions({
      method: RequestMethod.Get,
      url: this._apiPath + url,
      withCredentials: true,
      headers: headers,
      search: params
    });

    return this.execute(options);
  }

  getQueries(url: string, queries: any): Observable<Response> {
    const params: URLSearchParams = new URLSearchParams();
    Object.keys(queries || {}).forEach(prop => {
      params.set(prop, queries[prop]);
    });
    const options = new RequestOptions({
      search: params,
      withCredentials : true
    });
    return this.get(url, options);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {

    const reqOpts = options || {};
    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.body = reqOpts.body || '';
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Get;
    reqOpts.url = this._apiPath + url;

    return this.execute(reqOpts);
  }

  getWithData(url: string, data: any): Observable<Response> {
    const params: URLSearchParams = new URLSearchParams();
    Object.keys(data || {}).forEach(prop => {
      params.set(prop, data[prop]);
    });
    const options = new RequestOptions({
      search: params,
      withCredentials: true
    });
    return this.get(url, options);
  }

  postData(url: string, data: any) {
    const reqOpts: RequestOptionsArgs = {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Post;
    reqOpts.url = this._apiPath + url;
    reqOpts.body = data;
    return this.execute(reqOpts);
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    const reqOpts = options || {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Post;
    reqOpts.url = this._apiPath + url;
    reqOpts.body = body;

    return this.execute(reqOpts);
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    const reqOpts = options || {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Put;
    reqOpts.url = this._apiPath + url;
    reqOpts.body = body || '';

    return this.execute(reqOpts);
  }

  delete(url: string, data?, options?: RequestOptionsArgs): Observable<Response> {
    const reqOpts = options || {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Delete;
    reqOpts.url = this._apiPath + url;
    reqOpts.body = data;

    return this.execute(reqOpts);
  }
  patch(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    const reqOpts = options || {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Patch;
    reqOpts.url = this._apiPath + url;
    reqOpts.body = body;

    return this.execute(reqOpts);
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    const reqOpts = options || {};

    reqOpts.headers = reqOpts.headers ? new Headers(reqOpts.headers) : this.setHeaders();
    reqOpts.withCredentials = true;
    reqOpts.method = RequestMethod.Head;
    reqOpts.url = this._apiPath + url;

    return this.execute(reqOpts);
  }

  public setHeadersForUpLoadFile() {
    const authHeaders = new Headers();
    authHeaders.append('Authorization', 'bearer ' + this.TOKEN);
    // authHeaders.append('Content-Type', 'multipart/form-data');
    const options: RequestOptionsArgs = {
      headers: authHeaders,
      withCredentials: true
    };
    return options;
  }

  uploadFile(url: string, data: any): Observable<any> {
    const options = this.setHeadersForUpLoadFile();
    url = this._apiPath + url;
    return this.http.post(url, data, options);
  }

  private alertException(result: ResultDto) {
    if (result && result.message) {
      Common.throwErrorOrBadRequest(result.message);
    }
  }

}
