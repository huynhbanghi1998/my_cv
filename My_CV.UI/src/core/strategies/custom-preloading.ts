import { PreloadingStrategy, Route } from '@angular/router';
import { Observable } from 'rxjs/Observable';

export class CustomPreloading implements PreloadingStrategy {
    preload(route: Route, load: () => Observable<any>): Observable<any> {
        return route.data && route.data.preload ? load() : new Observable(observer => observer.next(null));
    }
}

export const customPreloadingProvider = { provide: PreloadingStrategy, useClass: CustomPreloading };
