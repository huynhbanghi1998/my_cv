/* SystemJS module definition */
interface ApiConfig {
    host: string;
    prefix: string,
    cache: boolean,
    cacheLanguage: boolean
  }
  declare var module: NodeModule;
  interface NodeModule {
    id: string;
  }
  interface NodeRequire {
    ensure:any;
  }
  
  interface Window { editor: any; }
  
  declare var __appConfig__: ApiConfig;
  declare var jQuery: any;
  declare const $: any;
  declare const swal: any;
  declare var _: any;
  declare var require: any;
  declare var mApp: any;
  declare var mLayout: any;
  declare var mScrollTop: any;
  declare var mQuicksearch: any;
  declare var mUtil: any;
  declare var mMenu: any;
  declare var mQuickSidebar: any;  
  declare var Dashboard: any; 
  declare var CKEDITOR: any; 