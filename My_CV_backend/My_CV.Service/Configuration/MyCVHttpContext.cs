﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using My_CV.Service.Constant;

namespace My_CV.Service.Configuration
{
    public class MyCVHttpContext
    {
        private static readonly AsyncLocal<HttpContext> _context = new AsyncLocal<HttpContext>();

        /// <summary>
        /// Get <see cref="HttpContext"/> in this request.
        /// </summary>
        public static HttpContext Current
        {
            get
            {
                if (_context.Value == null)
                {
                    throw new InvalidOperationException(
                        $"Could not acuire {nameof(HttpContext)} in the current request. You may not add CurrentRequestContextMiddleware in Startup.");
                }

                return _context.Value;
            }
            set { _context.Value = value; }
        }

        public static string UserName
        {
            get
            {
                var userName = String.Empty;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    userName = Current.User.FindFirst(MyCVConstant.UserName)?.Value;
                }

                return userName;
            }
        }

        public static string LoginUserId
        {
            get
            {
                string loginUserId = string.Empty;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    loginUserId = Current.User.FindFirst(MyCVConstant.LoginUserId)?.Value;
                }

                return loginUserId;
            }
        }

        public static string UserType
        {
            get
            {
                var userName = String.Empty;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    userName = Current.User.FindFirst(MyCVConstant.UserType)?.Value;
                }

                return userName;
            }
        }

        public static long UserRole
        {
            get
            {
                long userRole = 0;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    var role = Current.User.FindFirst(MyCVConstant.UserRole)?.Value;
                    long.TryParse(role, out userRole);
                }

                return userRole;
            }
        }

        public static string ConnectionId
        {
            get
            {
                string connectionId = string.Empty;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    connectionId = Current.User.FindFirst("ConnectionId")?.Value;
                }

                return connectionId;
            }
        }
        public static string ApiUrl { get; set; } = "";


        public static string ConnenctionString { get; set; } = "";

        public static string UploadPath { get; set; } = "";

        public static string Mail_From { get; set; } = "";
        public static string Mail_To { get; set; } = "";
        public static string Mail_FullName { get; set; } = "";
        public static string Mail_Password { get; set; } = "";
        public static int Mail_Port { get; set; } = 0;
        public static string Mail_Server { get; set; } = "";
        public static string Mail_UserName { get; set; } = "";
    }
}
