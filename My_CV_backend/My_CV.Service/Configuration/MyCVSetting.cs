﻿namespace My_CV.Service.Configuration
{
    public class MyCVSetting
    {
        public string AppName { get; set; }
        public string ConnectionString { get; set; }
        public string ClientUrl { get; set; }
        public string UPLOAD_PATH { get; set; }
    }
}
