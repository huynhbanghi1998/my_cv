﻿namespace My_CV.Service.DTO
{
    public class AccountRequestDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string NewPass { get; set; }
        public string OldPass { get; set; }
        public string ConfirmPass { get; set; }
    }
}
