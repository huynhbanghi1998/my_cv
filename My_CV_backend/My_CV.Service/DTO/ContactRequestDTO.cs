﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_CV.Service.DTO
{
    public class ContactRequestDTO
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
