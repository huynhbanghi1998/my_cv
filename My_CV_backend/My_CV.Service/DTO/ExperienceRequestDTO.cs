﻿namespace My_CV.Service.DTO
{
    public class ExperienceRequestDTO
    {
        public string ID { get; set; }
        public string Company { get; set; }
        public string Tittle { get; set; }
        public string Description { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
