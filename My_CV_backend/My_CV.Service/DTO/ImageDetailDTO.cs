﻿namespace My_CV.Service.DTO
{
    public class ImageDetailDTO
    {
        public string Url { get; set; }
        public string Thumb { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
