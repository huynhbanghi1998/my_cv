﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace My_CV.Service.DTO
{
    public class InformationRequestDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
        public string Skype { get; set; }
        public string Marital { get; set; }
        public string Phone { get; set; }
        public string Nationality { get; set; }
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string AvatarUrl { get; set; }
        public string CoverImage1Url { get; set; }
        public string CoverImage2Url { get; set; }

        public IList<IFormFile> FileImage { get; set; }
        public bool IsChangeImage { get; set; }
    }
}
