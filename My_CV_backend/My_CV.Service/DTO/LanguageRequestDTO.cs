﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_CV.Service.DTO
{
    public class LanguageRequestDTO
    {
        public string Id { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
        public string Flag { get; set; }
    }
}
