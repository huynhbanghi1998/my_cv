﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_CV.Service.DTO
{
    public class LoginRequestDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
    }
}
