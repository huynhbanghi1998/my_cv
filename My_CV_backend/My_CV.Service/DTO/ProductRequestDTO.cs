﻿using Microsoft.AspNetCore.Http;

namespace My_CV.Service.DTO
{
    public class ProductRequestDTO
    {
        public string Id { get; set; }
        public string ImgUrl { get; set; }
        public string ProductType { get; set; }
        public string Product_TypeID { get; set; }

        public IFormFile FileImage { get; set; }
        public bool IsChangeImage { get; set; }
    }
}
