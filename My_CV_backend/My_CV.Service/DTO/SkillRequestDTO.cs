﻿namespace My_CV.Service.DTO
{
    public class SkillRequestDTO
    {
        public string Id { get; set; }
        public string SkillName { get; set; }
        public string Proficiency { get; set; }
    }
}
