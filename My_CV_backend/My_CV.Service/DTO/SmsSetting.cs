﻿namespace My_CV.Service.DTO
{
    public class SmsSetting
    {
        public SmsSetting()
        {
        }

        public string SenderNumber { get; set; }
        public string SessionData { get; set; }
        public string CountryCode { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
