﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Accounts
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
