﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Educations
    {
        public Guid Id { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Tittle { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            string s = DateTime.Parse(DateFrom.ToString()).Year.ToString() + " - " + DateTime.Parse(DateTo.ToString()).Year.ToString();
            return s;
        }
    }
}
