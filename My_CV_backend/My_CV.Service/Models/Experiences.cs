﻿using System;

namespace My_CV.Service.Models
{
    public partial class Experiences
    {
        public Guid Id { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Company { get; set; }
        public string Tittle { get; set; }
        public string Description { get; set; }

        public string DateToMonthAndYear(string date)
        {
            return DateTime.Parse(date).Month.ToString() + "/" + DateTime.Parse(date).Year.ToString();
        }

        public string DifferentBetweenDay()
        {
            TimeSpan ts = (DateTime)DateTo - (DateTime)DateFrom;
            int year = ts.Days / 365;
            int month = (ts.Days % 365) / 30;
            string s = year > 0 ? year.ToString() + " years " + month.ToString() + " months" : s = month.ToString() + " months";
            return s;
        }

        public string postionWorked()
        {
            return this.Tittle + " at " + this.Company;
        }
    }
}
