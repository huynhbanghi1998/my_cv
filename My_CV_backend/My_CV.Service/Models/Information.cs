﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Information
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
        public string Skype { get; set; }
        public string Marital { get; set; }
        public string Phone { get; set; }
        public string Nationality { get; set; }
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string AvatarUrl { get; set; }
        public string CoverImage1Url { get; set; }
        public string CoverImage2Url { get; set; }

        public string partOneOfDescription() {
            return this.AboutMe.Substring(0, this.AboutMe.IndexOf("<br />")); }

        public string partTwoOfDescription() {
            return this.AboutMe.Substring(this.AboutMe.IndexOf("<br />") + 6, this.AboutMe.Length - (this.AboutMe.IndexOf("<br />") + 6)); }
    }
}
