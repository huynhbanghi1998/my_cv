﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Languages
    {
        public Guid Id { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
        public string Flag { get; set; }
    }
}
