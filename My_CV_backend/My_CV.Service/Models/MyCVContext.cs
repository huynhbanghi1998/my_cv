﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using My_CV.Service.Configuration;

namespace My_CV.Service.Models
{
    public partial class MyCVContext : DbContext
    {
        public MyCVContext()
        {
        }

        public MyCVContext(DbContextOptions<MyCVContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Educations> Educations { get; set; }
        public virtual DbSet<Experiences> Experiences { get; set; }
        public virtual DbSet<Information> Information { get; set; }
        public virtual DbSet<Languages> Languages { get; set; }
        public virtual DbSet<Recommendations> Recommendations { get; set; }
        public virtual DbSet<Skills> Skills { get; set; }
        public virtual DbSet<Products> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //    optionsBuilder.UseSqlServer("Server=DESKTOP-FTJSUGM\\SQLEXPRESS;Database=MyCV;Trusted_Connection=True;");
            //}
            var connect = MyCVHttpContext.ConnenctionString;
            optionsBuilder.UseSqlServer(connect);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.UserName).IsRequired();

                entity.Property(e => e.PassWord).IsRequired();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Educations>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");
            });

            modelBuilder.Entity<Experiences>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateFrom).HasColumnType("date");

                entity.Property(e => e.DateTo).HasColumnType("date");

                entity.Property(e => e.Tittle).HasMaxLength(50);
            });

            modelBuilder.Entity<Information>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AboutMe).IsRequired();

                entity.Property(e => e.AvatarUrl)
                    .IsRequired()
                    .HasColumnName("Avatar_URL");

                entity.Property(e => e.Birthday)
                    .IsRequired()
                    .HasColumnName("Birthday");

                entity.Property(e => e.CoverImage1Url)
                    .IsRequired()
                    .HasColumnName("CoverImage1_URL");

                entity.Property(e => e.CoverImage2Url)
                    .IsRequired()
                    .HasColumnName("CoverImage2_URL");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Marital)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Nationality)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Skype)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Languages>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Language).HasColumnName("Language");
                entity.Property(e => e.Description).HasColumnName("Description");
                entity.Property(e => e.Flag).HasColumnName("Flag");
            });

            modelBuilder.Entity<Recommendations>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Postion).HasMaxLength(50);
            });

            modelBuilder.Entity<Skills>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.SkillName).IsRequired();

                entity.Property(e => e.Proficiency).IsRequired();
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ImgUrl).HasColumnName("imgURL");

                entity.Property(e => e.ProductType)
                    .HasColumnName("Product_Type")
                    .HasMaxLength(50);

                entity.Property(e => e.Product_TypeID)
                    .HasColumnName("Product_TypeID")
                    .HasMaxLength(50);
            });
        }
    }
}
