﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_CV.Service.Models
{
    public partial class Products
    {
        public Guid Id { get; set; }
        public string ImgUrl { get; set; }
        public string ProductType { get; set; }
        public string Product_TypeID { get; set; }
    }
}
