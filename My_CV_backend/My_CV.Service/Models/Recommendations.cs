﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Recommendations
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Postion { get; set; }
        public string Description { get; set; }
    }
}
