﻿using System;
using System.Collections.Generic;

namespace My_CV.Service.Models
{
    public partial class Skills
    {
        public Guid Id { get; set; }
        public string SkillName { get; set; }
        public string Proficiency { get; set; }
    }
}
