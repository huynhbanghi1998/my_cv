﻿using My_CV.Service.Models;
using System;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IAccountQueryService
    {
        Task<Accounts> ValidateUser(string username, string password);
        Task<Accounts> getAccountById(Guid id);
        Task<bool> UpdateAccount(Accounts account);
    }
}
