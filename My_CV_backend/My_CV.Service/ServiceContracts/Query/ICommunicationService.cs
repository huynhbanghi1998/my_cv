﻿using My_CV.Service.DTO;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface ICommunicationService
    {
        void SendMail(string subject, string body, string mailFrom, string mailFromName, string mailTo, bool isBodyHtml = true, SmtpSetting smtpSetting = null);
        bool SendSms(string phoneno, string msg, ref string status, ref string reference, SmsSetting smsSetting = null);
    }
}
