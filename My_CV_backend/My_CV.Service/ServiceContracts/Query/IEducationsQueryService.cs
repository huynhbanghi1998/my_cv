﻿using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IEducationsQueryService
    {
        Task<List<Educations>> GetEducationInHome();
        Task<Educations> GetEducationByID(Guid id);
    }
}
