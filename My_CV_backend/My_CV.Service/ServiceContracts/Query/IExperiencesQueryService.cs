﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IExperiencesQueryService
    {
        Task<List<Experiences>> GetExperienceInHome();
        Task<Experiences> GetExperienceByID(Guid id);
        Task<bool> InsertExperience(Experiences courses);
        Task<bool> UpdateExperience(ExperienceRequestDTO request);
        Task<bool> DeleteExperience(string id);
    }
}
