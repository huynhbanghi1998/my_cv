﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IInformationQueryService
    {
        Task<List<Information>> GetInformationInHome();
        Task<bool> UpdateInformation(InformationRequestDTO information);
        Task<Information> GetInformationById(Guid id);
    }
}
