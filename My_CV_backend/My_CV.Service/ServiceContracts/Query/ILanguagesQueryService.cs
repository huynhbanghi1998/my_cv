﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface ILanguagesQueryService
    {
        Task<List<Languages>> GetLanguageInHome();
        Task<Languages> GetLanguageById(Guid id);
        Task<bool> InsertLanguage(Languages courses);
        Task<bool> UpdateLanguage(LanguageRequestDTO request);
        Task<bool> DeleteLanguage(string id);
    }
}
