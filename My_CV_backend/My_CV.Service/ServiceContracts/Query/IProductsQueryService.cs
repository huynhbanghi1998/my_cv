﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IProductsQueryService
    {
        Task<List<Products>> GetProductInHome();
        Task<Products> GetProductByID(Guid id);
        Task<List<Products>> getProductType();
        Task<bool> InsertProduct(Products courses);
        Task<bool> UpdateProduct(ProductRequestDTO request);
        Task<bool> DeleteProduct(string id);
    }
}
