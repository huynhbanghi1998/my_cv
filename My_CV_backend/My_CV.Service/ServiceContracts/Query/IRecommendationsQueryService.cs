﻿using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface IRecommendationsQueryService
    {
        Task<Recommendations> GetRecommendationByID(Guid id);
        Task<List<Recommendations>> GetRecommendationInHome();
    }
}
