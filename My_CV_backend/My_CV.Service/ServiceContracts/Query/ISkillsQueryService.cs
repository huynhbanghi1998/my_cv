﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.Service.ServiceContracts.Query
{
    public interface ISkillsQueryService
    {
        Task<List<Skills>> GetSkillInHome();
        Task<Skills> GetSkillByID(Guid id);
        Task<bool> InsertSkill(Skills skill);
        Task<bool> UpdateSkill(SkillRequestDTO request);
        Task<bool> DeleteSkill(string id);
    }
}
