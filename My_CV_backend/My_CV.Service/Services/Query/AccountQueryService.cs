﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My_CV.Service.Configuration;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.Service.Services.Query
{
    public class AccountQueryService : IAccountQueryService
    {
        public AccountQueryService()
        {
        }

        public async Task<Accounts> ValidateUser(string username, string password)
        {
            try
            {
                using (var db = new MyCVContext())
                {
                    var isValidUser = db.Accounts.FirstOrDefault(item => item.UserName == username && item.PassWord == password);
                    return isValidUser;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<Accounts> getAccountById(Guid id)
        {
            try
            {
                using (var db = new MyCVContext())
                {
                    var isValidUser = db.Accounts.FirstOrDefault(item => item.Id == id);
                    return isValidUser;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> UpdateAccount(Accounts account)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    _context.Accounts.Update(account);
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
