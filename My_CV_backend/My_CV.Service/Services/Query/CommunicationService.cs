﻿using My_CV.Service.Configuration;
using My_CV.Service.DTO;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Net;
using System.Net.Mail;

namespace My_CV.Service.Services.Query
{
    public class CommunicationService : ICommunicationService
    {
        public void SendMail(string subject, string body, string mailFrom, string mailFromName, string mailTo, bool isBodyHtml = true, SmtpSetting smtpSetting = default(SmtpSetting))
        {
            try
            {
                smtpSetting = smtpSetting ?? GetSmtpSetting();
                SmtpClient client = new SmtpClient(smtpSetting.Server, smtpSetting.Port);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(smtpSetting.UserName, smtpSetting.Password);
                MailMessage mailMessage = new MailMessage();
                if (string.IsNullOrEmpty(mailFrom))
                {
                    mailMessage.From = new MailAddress(smtpSetting.From);
                }
                else
                {
                    mailMessage.From = new MailAddress(mailFrom, mailFromName);
                    mailMessage.Sender = new MailAddress(mailFrom, mailFromName);
                }
                if (!string.IsNullOrEmpty(mailTo))
                {
                    mailMessage.To.Add(mailTo);
                }
                else
                {
                    mailMessage.To.Add(smtpSetting.To);
                }

                mailMessage.Body = body;
                mailMessage.IsBodyHtml = isBodyHtml;
                mailMessage.Subject = subject;
                client.Send(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public bool SendSms(string phoneno, string msg, ref string status, ref string reference, SmsSetting smsSetting = null)
        {
            throw new NotImplementedException();
        }

        private SmtpSetting GetSmtpSetting()
        {
            return new SmtpSetting
            {
                EnableSsl = true,
                From = MyCVHttpContext.Mail_From,
                To = MyCVHttpContext.Mail_To,
                FullName = MyCVHttpContext.Mail_FullName,
                Password = MyCVHttpContext.Mail_Password,
                Port = MyCVHttpContext.Mail_Port,
                Server = MyCVHttpContext.Mail_Server,
                UserName = MyCVHttpContext.Mail_UserName
            };
        }
    }
}
