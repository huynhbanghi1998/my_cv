﻿using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class EducationsQueryService : IEducationsQueryService
    {
        public async Task<Educations> GetEducationByID(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Educations educations = _context.Educations.FirstOrDefault(item => item.Id == id);
                    return educations;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Task<List<Educations>> GetEducationInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Educations> listEducations = _context.Educations.ToList();
                    return Task.FromResult(listEducations);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Educations>());
            }

        }
    }
}
