﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class ExperiencesQueryService : IExperiencesQueryService
    {
        public async Task<Experiences> GetExperienceByID(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Experiences experiences = _context.Experiences.FirstOrDefault(item => item.Id == id);
                    return experiences;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        // sort experience list to datetime ascending
        class dateTime
        {
            public string id { get; set; }
            public int day { get; set; }
            public int month { get; set; }
            public int year { get; set; }
        }

        private List<Experiences> sortListToDatetimeAscending(List<Experiences> _list)
        {
            List<dateTime> _listDateTime = new List<dateTime>();
            foreach (var item in _list)
            {
                dateTime tmp = new dateTime();
                tmp.id = item.Id.ToString();
                tmp.day = DateTime.Parse(item.DateTo.ToString()).Day;
                tmp.month = DateTime.Parse(item.DateTo.ToString()).Month;
                tmp.year = DateTime.Parse(item.DateTo.ToString()).Year;
                _listDateTime.Add(tmp);
            }
            for (int i = 0; i < _listDateTime.Count - 1; i++)
                for (int j = i + 1; j < _listDateTime.Count; j++)
                {
                    if (_listDateTime[i].year > _listDateTime[j].year)
                    {
                        var temp = _listDateTime[i].id == _list[i].Id.ToString() ? _list[i] : _list[j];
                        _list[i] = _list[j];
                        _list[j] = temp;
                    }
                    else if (_listDateTime[i].year == _listDateTime[j].year)
                    {
                        if (_listDateTime[i].month > _listDateTime[j].month)
                        {
                            var temp = _listDateTime[i].id == _list[i].Id.ToString() ? _list[i] : _list[j];
                            _list[i] = _list[j];
                            _list[j] = temp;
                        }
                    }
                }

            return _list;
        }
        //

        public Task<List<Experiences>> GetExperienceInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Experiences> listExperiences = sortListToDatetimeAscending(_context.Experiences.ToList());

                    return Task.FromResult(listExperiences);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Experiences>());
            }

        }

        public async Task<bool> InsertExperience(Experiences experience)
        {
            using (var _context = new MyCVContext())
            {
                _context.Experiences.Add(experience);
                _context.SaveChanges();
                return true;
            }
        }

        public async Task<bool> UpdateExperience(ExperienceRequestDTO request)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Experiences.FirstOrDefault(item => item.Id == Guid.Parse(request.ID));
                    temp.DateFrom = DateTime.Parse(request.DateFrom);
                    temp.DateTo = DateTime.Parse(request.DateTo);
                    temp.Company = request.Company;
                    temp.Tittle = request.Tittle;
                    temp.Description = request.Description;
                    _context.Experiences.Update(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public async Task<bool> DeleteExperience(string id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Experiences.FirstOrDefault(item => item.Id == Guid.Parse(id));
                    _context.Experiences.Remove(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
