﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class InformationQueryService : IInformationQueryService
    {
        public InformationQueryService() { }

        public Task<List<Information>> GetInformationInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Information> listInfo = _context.Information.ToList();
                    return Task.FromResult(listInfo);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Information>());
            }
        }

        public async Task<bool> UpdateInformation(InformationRequestDTO request)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Information.FirstOrDefault(item => item.Id == Guid.Parse(request.Id));
                    temp.AboutMe = request.AboutMe;
                    if (request.AvatarUrl != null || request.CoverImage1Url != null || request.CoverImage2Url != null)
                    {
                        temp.AvatarUrl = request.AvatarUrl;
                        temp.CoverImage1Url = request.CoverImage1Url;
                        temp.CoverImage2Url = request.CoverImage2Url;
                    }
                    temp.Name = request.Name;
                    temp.Birthday = request.Birthday;
                    temp.Skype = request.Skype;
                    temp.Marital = request.Marital;
                    temp.Phone = request.Phone;
                    temp.Nationality = request.Nationality;
                    temp.Email = request.Email;
                    _context.Information.Update(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<Information> GetInformationById(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Information course = _context.Information.FirstOrDefault(item => item.Id == id);
                    return course;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
