﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class LanguagesQueryService : ILanguagesQueryService
    {
        public Task<List<Languages>> GetLanguageInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Languages> listInfo = _context.Languages.ToList();
                    return Task.FromResult(listInfo);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Languages>());
            }
        }

        public async Task<Languages> GetLanguageById(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Languages language = _context.Languages.FirstOrDefault(item => item.Id == id);
                    return language;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<bool> InsertLanguage(Languages language)
        {
            using (var _context = new MyCVContext())
            {
                _context.Languages.Add(language);
                _context.SaveChanges();
                return true;
            }
        }

        public async Task<bool> UpdateLanguage(LanguageRequestDTO request)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Languages.FirstOrDefault(item => item.Id == Guid.Parse(request.Id));
                    temp.Language = request.Language;
                    temp.Description = request.Description;
                    temp.Flag = request.Flag;
                    _context.Languages.Update(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public async Task<bool> DeleteLanguage(string id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Languages.FirstOrDefault(item => item.Id == Guid.Parse(id));
                    _context.Languages.Remove(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
