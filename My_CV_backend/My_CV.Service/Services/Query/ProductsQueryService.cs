﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class ProductsQueryService : IProductsQueryService
    {
        public async Task<Products> GetProductByID(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Products Products = _context.Products.FirstOrDefault(item => item.Id == id);
                    return Products;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Task<List<Products>> GetProductInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Products> listProducts = _context.Products.ToList();
                    return Task.FromResult(listProducts);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Products>());
            }

        }

        public Task<List<Products>> getProductType()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Products> listProducts = getProductType(_context.Products.ToList());
                    return Task.FromResult(listProducts);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Products>());
            }
        }

        private List<Products> getProductType(List<Products> _list)
        {
            List<Products> listProductType = new List<Products>();
            for (int i = 0; i < _list.Count; i++)
            {
                if (checkExisting(listProductType, _list[i]))
                    listProductType.Add(_list[i]);
            }

            return listProductType;
        }

        private bool checkExisting(List<Products> listProductType, Products Product)
        {
            for (int i = 0; i < listProductType.Count; i++)
            {
                if (listProductType[i].Product_TypeID == Product.Product_TypeID)
                    return false;
            }

            return true;
        }

        public async Task<bool> InsertProduct(Products product)
        {
            using (var _context = new MyCVContext())
            {
                _context.Products.Add(product);
                _context.SaveChanges();
                return true;
            }
        }

        public async Task<bool> UpdateProduct(ProductRequestDTO request)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Products.FirstOrDefault(item => item.Id == Guid.Parse(request.Id));
                    if (request.ImgUrl != null)
                        temp.ImgUrl = request.ImgUrl;
                    temp.ProductType = request.ProductType;
                    temp.Product_TypeID = request.Product_TypeID;
                    _context.Products.Update(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> DeleteProduct(string id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Products.FirstOrDefault(item => item.Id == Guid.Parse(id));
                    _context.Products.Remove(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
