﻿using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class RecommendationsQueryService : IRecommendationsQueryService
    {
        public async Task<Recommendations> GetRecommendationByID(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Recommendations recommendations = _context.Recommendations.FirstOrDefault(item => item.Id == id);
                    return recommendations;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Task<List<Recommendations>> GetRecommendationInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Recommendations> listRecommendations = _context.Recommendations.ToList();
                    return Task.FromResult(listRecommendations);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Recommendations>());
            }

        }
    }
}
