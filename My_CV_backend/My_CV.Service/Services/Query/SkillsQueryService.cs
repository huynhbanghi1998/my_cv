﻿using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_CV.Service.Services.Query
{
    public class SkillsQueryService : ISkillsQueryService
    {
        public async Task<Skills> GetSkillByID(Guid id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    Skills skills = _context.Skills.FirstOrDefault(item => item.Id == id);
                    return skills;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Task<List<Skills>> GetSkillInHome()
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    List<Skills> listSkills = _context.Skills.ToList();
                    return Task.FromResult(listSkills);
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new List<Skills>());
            }

        }

        public async Task<bool> InsertSkill(Skills skill)
        {
            using (var _context = new MyCVContext())
            {
                _context.Skills.Add(skill);
                _context.SaveChanges();

                return true;
            }
        }

        public async Task<bool> UpdateSkill(SkillRequestDTO request)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Skills.FirstOrDefault(item => item.Id == Guid.Parse(request.Id));
                    temp.SkillName = request.SkillName;
                    temp.Proficiency = request.Proficiency;
                    _context.Skills.Update(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteSkill(string id)
        {
            try
            {
                using (var _context = new MyCVContext())
                {
                    var temp = _context.Skills.FirstOrDefault(item => item.Id == Guid.Parse(id));
                    _context.Skills.Remove(temp);
                    _context.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
