﻿using Microsoft.Extensions.DependencyInjection;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using My_CV.Service.Services.Query;

namespace My_CV.Service
{
    public class WebIocRegister
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<MyCVContext, MyCVContext>();
            services.AddSingleton<IAccountQueryService, AccountQueryService>();
            services.AddSingleton<IInformationQueryService, InformationQueryService>();
            services.AddSingleton<ICommunicationService, CommunicationService>();
            services.AddSingleton<IEducationsQueryService, EducationsQueryService>();
            services.AddSingleton<IExperiencesQueryService, ExperiencesQueryService>();
            services.AddSingleton<IRecommendationsQueryService, RecommendationsQueryService>();
            services.AddSingleton<ISkillsQueryService, SkillsQueryService>();
            services.AddSingleton<ILanguagesQueryService, LanguagesQueryService>();
            services.AddSingleton<IProductsQueryService, ProductsQueryService>();
        }
    }
}
