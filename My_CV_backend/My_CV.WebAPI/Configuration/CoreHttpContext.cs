﻿using Microsoft.AspNetCore.Http;
using My_CV.Service.Constant;
using System;
using System.Threading;

namespace My_CV.WebAPI.Configuration
{
    public class CoreHttpContext
    {
        private static readonly AsyncLocal<HttpContext> _context = new AsyncLocal<HttpContext>();

        /// <summary>
        /// Get <see cref="HttpContext"/> in this request.
        /// </summary>
        public static HttpContext Current
        {
            get
            {
                if (_context.Value == null)
                {
                    throw new InvalidOperationException(
                        $"Could not acuire {nameof(HttpContext)} in the current request. You may not add CurrentRequestContextMiddleware in Startup.");
                }

                return _context.Value;
            }
            set { _context.Value = value; }
        }

        public static string UserName
        {
            get
            {
                var userName = String.Empty;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    userName = Current.User.FindFirst(MyCVIdentityClaims.UserName)?.Value;
                }

                return userName;
            }
        }

        public static Guid LoginUserId
        {
            get
            {
                Guid loginUserId = new Guid();
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    var value = Current.User.FindFirst(MyCVIdentityClaims.LoginUserId)?.Value;
                    if (!string.IsNullOrEmpty(value))
                    {
                        loginUserId = Guid.Parse(value);
                    }
                }

                return loginUserId;
            }
        }

        public static int UserType
        {
            get
            {
                var userType = 0;
                // Figure out the user's identity
                var identity = Current?.User?.Identity;

                if (identity != null && identity.IsAuthenticated)
                {
                    var value = Current.User.FindFirst(MyCVIdentityClaims.UserType)?.Value;
                    if (!string.IsNullOrEmpty(value))
                    {
                        userType = Int32.Parse(value);
                    }
                }

                return userType;
            }
        }
    }
}