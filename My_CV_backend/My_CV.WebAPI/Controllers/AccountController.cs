﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using My_CV.Service.DTO;
using My_CV.Service.ServiceContracts.Query;
using My_CV.Service.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using My_CV.Service.Constant;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using My_CV.WebAPI.Configuration;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountQueryService _accountQueryService;
        private readonly IConfiguration _configuration;

        public AccountController(
            IAccountQueryService accountQueryService,
            IConfiguration configuration)
        {
            _accountQueryService = accountQueryService;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> RequestToken([FromBody] LoginRequestDTO request)
        {
            var result = await _accountQueryService.ValidateUser(request.Username, request.Password);
            if (result != null)
            {
                var token = await GenerateJwtToken(result);
                return Ok(new
                {
                    status = "success",
                    result = new
                    {
                        access_token = token,
                        info = new
                        {
                            name = result.UserName,
                            id = result.Id,
                        }
                    }
                });
            }
            else
            {
                return Ok(new
                {
                    status = "warning",
                    result = "Incorrect username or password"
                });
            }

        }

        private async Task<object> GenerateJwtToken(Accounts user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.NameIdentifier, user.UserName),
                new Claim(MyCVIdentityClaims.UserName, user.UserName),
                new Claim(MyCVIdentityClaims.UserType, user.UserName),
                new Claim(MyCVIdentityClaims.LoginUserId, user.Id.ToString())
            };


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.UtcNow.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JwtIssuer"],
                audience: _configuration["JwtIssuer"],
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        [Authorize]
        [HttpGet("isLogin")]
        public async Task<IActionResult> isLogin()
        {
            return Ok(true);
        }

        [Authorize]
        [HttpPost("update-pass-word")]
        public async Task<IActionResult> UpdatePassword([FromBody]AccountRequestDTO account)
        {
            var userId = CoreHttpContext.LoginUserId;
            var accountExist = await _accountQueryService.getAccountById(userId);
            if (accountExist != null)
            {
                if (accountExist.PassWord == account.OldPass)
                {
                    if (account.NewPass == account.ConfirmPass)
                    {
                        accountExist.PassWord = account.NewPass;
                        var result = await _accountQueryService.UpdateAccount(accountExist);
                        return Ok(new
                        {
                            status = "success"
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            status = "fail",
                            data = "New pass and confirm pass aren't correct"
                        });
                    }
                }
                else
                {
                    return Ok(new
                    {
                        status = "fail",
                        data = "PassWord is wrong. Please input correct old password!"
                    });
                }
            }
            else
            {
                return Ok(new
                {
                    status = "fail",
                    data = "User not exist"
                });
            }
        }

    }
}