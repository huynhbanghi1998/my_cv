﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/experience")]
    [ApiController]
    public class ExperienceController : ControllerBase
    {
        private readonly IExperiencesQueryService _experiencesQueryService;
        public ExperienceController(IExperiencesQueryService experiencesQueryService)
        {
            _experiencesQueryService = experiencesQueryService;
        }
        [HttpGet("get-experience-list")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listCourses = await _experiencesQueryService.GetExperienceInHome();
                return Ok(new
                {
                    data = listCourses,
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    data = new List<Experiences>(),
                    status = "fail"
                });
            }

        }

        [HttpGet("get-experience-by-id")]
        public async Task<IActionResult> getExperienceById([FromQuery] string id)
        {
            try
            {
                var course = await this._experiencesQueryService.GetExperienceByID(Guid.Parse(id));
                return Ok(new
                {
                    status = "success",
                    data = course
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }

        }
        [HttpPost("insert-experience")]
        public async Task<IActionResult> InsertExperience([FromForm] ExperienceRequestDTO request)
        {
            try
            {
                Experiences newExperience = new Experiences();
                newExperience.Id = Guid.NewGuid();
                newExperience.DateFrom = DateTime.Parse(request.DateFrom);
                newExperience.DateTo = DateTime.Parse(request.DateTo);
                newExperience.Company = request.Company;
                newExperience.Tittle = request.Tittle;
                newExperience.Description = request.Description;
                bool result = await _experiencesQueryService.InsertExperience(newExperience);

                if (result == true)
                {
                    return Ok(new
                    {
                        status = "success"
                    });
                }
                else
                    return Ok(new
                    {
                        status = "fail"
                    });


            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error"
                });
            }
        }
        [HttpPost("update-experience")]
        public async Task<IActionResult> UpdateExperience([FromForm] ExperienceRequestDTO request)
        {
            try
            {
                var result = _experiencesQueryService.UpdateExperience(request);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
        [HttpGet("delete-experience")]
        public async Task<IActionResult> DeleteExperience([FromQuery] string id)
        {
            try
            {
                var result = _experiencesQueryService.DeleteExperience(id);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
    }
}