﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using My_CV.Service.Configuration;
using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/information")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly IInformationQueryService _informationQueryService;
        public CoursesController(IInformationQueryService informationQueryService)
        {
            _informationQueryService = informationQueryService;
        }
        [HttpGet("get-information")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listInformation = await _informationQueryService.GetInformationInHome();
                return Ok(new
                {
                    data = listInformation,
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    data = new List<Information>(),
                    status = "fail"
                });
            }

        }

        [HttpPost("update-information")]
        public async Task<IActionResult> UpdateInformation([FromForm] InformationRequestDTO request)
        {
            try
            {
                if (request.FileImage != null)
                {
                    //string filePath = Path.Combine(MyCVHttpContext.UploadPath, "about-me", request.FileImage.FileName);

                    //using (var stream = new FileStream(filePath, FileMode.Create))
                    //{
                    //    request.FileImage.CopyTo(stream);
                    //    request.AvatarUrl = $"about-me/{request.FileImage.FileName}";
                    //    request.CoverImage1Url = $"about-me/{request.FileImage.FileName}";
                    //    request.CoverImage2Url = $"about-me/{request.FileImage.FileName}";
                    //}
                }
                else
                {
                    request.AvatarUrl = null;
                    request.CoverImage1Url = null;
                    request.CoverImage2Url = null;
                    var result1 = _informationQueryService.UpdateInformation(request);

                    return Ok(new
                    {
                        status = "success"
                    });
                }

                var imageOfInformation = await _informationQueryService.GetInformationById(Guid.Parse(request.Id));
                if (!string.IsNullOrEmpty(imageOfInformation.AvatarUrl) && !string.IsNullOrEmpty(imageOfInformation.CoverImage1Url) && !string.IsNullOrEmpty(imageOfInformation.CoverImage2Url))
                {
                    string oldAvatar = Path.Combine(MyCVHttpContext.UploadPath, imageOfInformation.AvatarUrl);
                    string oldCover1 = Path.Combine(MyCVHttpContext.UploadPath, imageOfInformation.CoverImage1Url);
                    string oldCover2 = Path.Combine(MyCVHttpContext.UploadPath, imageOfInformation.CoverImage2Url);

                    if (System.IO.File.Exists(oldAvatar) || System.IO.File.Exists(oldCover1) || System.IO.File.Exists(oldCover2))
                    {
                        System.IO.File.Delete(oldAvatar);
                        System.IO.File.Delete(oldCover1);
                        System.IO.File.Delete(oldCover2);
                    }
                }

                var result = _informationQueryService.UpdateInformation(request);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
    }
}