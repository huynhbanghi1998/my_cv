﻿using Microsoft.AspNetCore.Mvc;
using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/language")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private readonly ILanguagesQueryService _languagesQueryService;
        public LanguageController(ILanguagesQueryService languagesQueryService)
        {
            _languagesQueryService = languagesQueryService;
        }
        [HttpGet("get-language-list")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listLanguages = await _languagesQueryService.GetLanguageInHome();
                return Ok(new
                {
                    data = listLanguages,
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    data = new List<Languages>(),
                    status = "fail"
                });
            }

        }

        [HttpGet("get-language-by-id")]
        public async Task<IActionResult> getExperienceById([FromQuery] string id)
        {
            try
            {
                var language = await this._languagesQueryService.GetLanguageById(Guid.Parse(id));
                return Ok(new
                {
                    status = "success",
                    data = language
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }

        }
        [HttpPost("insert-language")]
        public async Task<IActionResult> InsertExperience([FromForm] LanguageRequestDTO request)
        {
            try
            {
                Languages newLanguage = new Languages();
                newLanguage.Id = Guid.NewGuid();
                newLanguage.Description = request.Description;
                newLanguage.Flag = request.Flag;
                newLanguage.Language = request.Language;

                bool result = await _languagesQueryService.InsertLanguage(newLanguage);

                if (result == true)
                {
                    return Ok(new
                    {
                        status = "success"
                    });
                }
                else
                    return Ok(new
                    {
                        status = "fail"
                    });


            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error"
                });
            }
        }

        [HttpPost("update-language")]
        public async Task<IActionResult> UpdateExperience([FromForm] LanguageRequestDTO request)
        {
            try
            {
                var result = _languagesQueryService.UpdateLanguage(request);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }

        [HttpGet("delete-language")]
        public async Task<IActionResult> DeleteExperience([FromQuery] string id)
        {
            try
            {
                var result = _languagesQueryService.DeleteLanguage(id);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
    }
}