﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using My_CV.Service.Configuration;
using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductsQueryService _productsQueryService;
        public ProductController(IProductsQueryService productsQueryService)
        {
            _productsQueryService = productsQueryService;
        }
        [HttpGet("get-product-list")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listProduct = await _productsQueryService.GetProductInHome();
                return Ok(new
                {
                    data = listProduct,
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    data = new List<Products>(),
                    status = "fail"
                });
            }

        }

        [HttpGet("get-product-by-id")]
        public async Task<IActionResult> GetProductById([FromQuery] string id)
        {
            try
            {
                var course = await this._productsQueryService.GetProductByID(Guid.Parse(id));
                return Ok(new
                {
                    status = "success",
                    data = course
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }

        }
        [HttpPost("insert-product")]
        public async Task<IActionResult> InsertProduct([FromForm] ProductRequestDTO request)
        {
            try
            {
                if (request.FileImage != null)
                {
                    string filePath = Path.Combine(MyCVHttpContext.UploadPath, "project", request.FileImage.FileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        request.FileImage.CopyTo(stream);
                    }
                }

                Products newProduct = new Products();
                newProduct.Id = Guid.NewGuid();
                newProduct.ImgUrl = $"project/{request.FileImage.FileName}";
                newProduct.ProductType = request.ProductType;
                newProduct.Product_TypeID = request.Product_TypeID;
                bool result = await _productsQueryService.InsertProduct(newProduct);

                if (result == true)
                {
                    return Ok(new
                    {
                        status = "success"
                    });
                }
                else
                    return Ok(new
                    {
                        status = "fail"
                    });


            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error"
                });
            }
        }
        [HttpPost("update-product")]
        public async Task<IActionResult> UpdateProduct([FromForm] ProductRequestDTO request)
        {
            try
            {

                if (request.FileImage != null)
                {
                    string filePath = Path.Combine(MyCVHttpContext.UploadPath, "project", request.FileImage.FileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        request.FileImage.CopyTo(stream);
                        request.ImgUrl = $"project/{request.FileImage.FileName}";
                    }
                }
                else
                {
                    request.ImgUrl = null;
                    var result1 = _productsQueryService.UpdateProduct(request);

                    return Ok(new
                    {
                        status = "success"
                    });
                }

                var backgroundImage = await _productsQueryService.GetProductByID(Guid.Parse(request.Id));
                if (!string.IsNullOrEmpty(backgroundImage.ImgUrl) && request.ImgUrl != backgroundImage.ImgUrl)
                {
                    string oldFile = Path.Combine(MyCVHttpContext.UploadPath, backgroundImage.ImgUrl);
                    if (System.IO.File.Exists(oldFile))
                    {
                        System.IO.File.Delete(oldFile);
                    }
                }


                var result = _productsQueryService.UpdateProduct(request);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
        [HttpGet("delete-product")]
        public async Task<IActionResult> DeleteProduct([FromQuery] string id)
        {
            try
            {

                var backgroundImage = await _productsQueryService.GetProductByID(Guid.Parse(id));
                if (!string.IsNullOrEmpty(backgroundImage.ImgUrl))
                {
                    string oldFile = Path.Combine(MyCVHttpContext.UploadPath, backgroundImage.ImgUrl);
                    if (System.IO.File.Exists(oldFile))
                    {
                        System.IO.File.Delete(oldFile);
                    }
                }
                var result = _productsQueryService.DeleteProduct(id);


                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
    }
}