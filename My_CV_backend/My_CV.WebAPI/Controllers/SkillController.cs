﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using My_CV.Service.DTO;
using My_CV.Service.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/skill")]
    [ApiController]
    public class SkillController : ControllerBase
    {
        private readonly ISkillsQueryService _skillsQueryService;
        public SkillController(ISkillsQueryService skillsQueryService)
        {
            _skillsQueryService = skillsQueryService;
        }

        [HttpGet("get-skill-list")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listSkill = await _skillsQueryService.GetSkillInHome();
                return Ok(new
                {
                    data = listSkill,
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    data = new List<Skills>(),
                    status = "fail"
                });
            }
        }

        [HttpGet("get-skill-by-id")]
        public async Task<IActionResult> getSkillById([FromQuery] string id)
        {
            try
            {
                var skill = await this._skillsQueryService.GetSkillByID(Guid.Parse(id));
                return Ok(new
                {
                    status = "success",
                    data = skill
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail",
                });
            }
        }

        [HttpPost("insert-skill")]
        public async Task<IActionResult> InsertSKill([FromForm] SkillRequestDTO request)
        {
            try
            {
                Skills newSkill = new Skills();
                newSkill.Id = Guid.NewGuid();
                newSkill.SkillName = request.SkillName;
                newSkill.Proficiency = request.Proficiency;

                bool result = await this._skillsQueryService.InsertSkill(newSkill);

                if (result == true)
                {
                    return Ok(new
                    {
                        status = "success"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        status = "fail"
                    });
                }
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "error"
                });
            }
        }

        [HttpPost("update-skill")]
        public async Task<IActionResult> UpdateSkill([FromForm] SkillRequestDTO request)
        {
            try
            {
                var result = this._skillsQueryService.UpdateSkill(request);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }

        [HttpGet("delete-skill")]
        public async Task<IActionResult> DeleteSkill([FromQuery] string id)
        {
            try
            {
                var result = this._skillsQueryService.DeleteSkill(id);

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "fail"
                });
            }
        }
    }
}