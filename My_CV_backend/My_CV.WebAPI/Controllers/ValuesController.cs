﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using My_CV.Service.Configuration;
using My_CV.Service.DTO;
using My_CV.Service.ServiceContracts.Query;
using My_CV.WebAPI.Utilities;
using Newtonsoft.Json;
using Serilog;

namespace My_CV.WebAPI.Controllers
{
    [Route("api/values")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ICommunicationService _communicationService;
        public ValuesController(ICommunicationService communicationService)
        {
            _communicationService = communicationService;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost("UploadImage")]
        public ActionResult<IEnumerable<string>> UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0) return null;
            if (!upload.IsImage())
            {
                var NotImageMessage = "please choose a picture";
                dynamic NotImage = JsonConvert.DeserializeObject("{ 'uploaded': 0, 'error': { 'message': \"" + NotImageMessage + "\"}}");
                return Ok(NotImage);
            }

            var fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();

            Image image = Image.FromStream(upload.OpenReadStream());


            if (upload.Length > 50000000)
            {
                var LengthErrorMessage = "File Max Size 50MB";
                dynamic stuff = JsonConvert.DeserializeObject("{ 'uploaded': 0, 'error': { 'message': \"" + LengthErrorMessage + "\"}}");
                return Ok(stuff);
            }

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/images/CKEditorImages",
                fileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                upload.CopyTo(stream);
            }

            var url = $"{"http://localhost:5000/images/CKEditorImages/"}{fileName}";
            var successMessage = "image is uploaded successfully";
            dynamic success = JsonConvert.DeserializeObject("{ 'uploaded': 1,'fileName': \"" + fileName + "\",'url': \"" + url + "\", 'error': { 'message': \"" + successMessage + "\"}}");
            return Ok(success);
        }

        [HttpGet("get-file-list")]
        public async Task<IActionResult> GetFileList()
        {
            List<string> fileList = new List<string>();
            string path = MyCVHttpContext.UploadPath;
            foreach (string file in Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories))
            {
                // do something
                string fileName = file.Replace(path + "\\", "");
                fileList.Add(fileName);
            }
            return Ok(fileList);
        }

        [HttpGet("delete-file-item")]
        public async Task<IActionResult> DeleteFileImage(string fileName)
        {
            try
            {
                string filePath = Path.Combine(MyCVHttpContext.UploadPath, fileName);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                    return Ok(new
                    {
                        status = "success"
                    });
                }

                return Ok(new
                {
                    status = "fail",
                    data = "file not exists"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Ok(new
                {
                    status = "fail",
                    data = e.Message
                });
            }

        }

        [HttpGet("get-dictionary-image")]
        public async Task<IActionResult> GetDictionaryImage()
        {
            try
            {
                string filePath = Path.Combine(MyCVHttpContext.UploadPath, "Content");
                var fileList = Directory.GetFiles(filePath);
                var a = new List<ImageDetailDTO>();
                foreach (var file in fileList)
                {
                    string fileName = file.Replace(filePath, "");
                    a.Add(new ImageDetailDTO() { Name = fileName, Tag = fileName, Thumb = "http://localhost:5000/image/Content/" + fileName, Url = "http://localhost:5000/image/Content/" + fileName });
                }



                return Ok(a);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Ok(new
                {
                    status = "fail",
                    data = e.Message
                });
            }

        }

        [HttpPost("upload_image_to_dictionary")]
        public async Task<IActionResult> GetDictionaryImage([FromForm] IFormFile item)
        {
            try
            {
                //var file = Request.Form["fileImage"];
                var file = Request.Form.Files[0];
                if (file != null)
                {
                    string filePath = Path.Combine(MyCVHttpContext.UploadPath, "Content", file.FileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                return Ok(new { link = "http://localhost:5000/image/Content/" + file.FileName });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Ok(new
                {
                    status = "fail",
                    data = e.Message
                });
            }

        }

        [HttpPost("delete-dictionary-image")]
        public async Task<IActionResult> DeleteDictionaryImage([FromForm] string name)
        {
            try
            {
                var fileName = name.Replace("\\", "");
                string oldFile = Path.Combine(MyCVHttpContext.UploadPath, "Content", fileName);
                if (System.IO.File.Exists(oldFile))
                {
                    System.IO.File.Delete(oldFile);
                }

                return Ok(new
                {
                    status = "success"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Ok(new
                {
                    status = "fail",
                    data = e.Message
                });
            }

        }

        [AllowAnonymous]
        [HttpPost("send-mail")]
        public async Task<IActionResult> SendMail([FromForm]ContactRequestDTO request)
        {
            try
            {
                string message = $@"<p>Email người gửi: {request.Email}</p></br>
                                <p>{request.Message}</p>";
                _communicationService.SendMail(request.Subject, message, request.Email, request.Name, "", true);
                return Ok(true);
                Log.Information($"{request.Email}: send => {request.Message}");
            }
            catch (Exception e)
            {
                Log.Error("ValuesController: SendMail =>" + e.Message);
                if (e.InnerException != null)
                {
                    Log.Error("ValuesController: SendMail =>" + e.InnerException.Message);
                }

                throw;
            }

        }


    }
}
