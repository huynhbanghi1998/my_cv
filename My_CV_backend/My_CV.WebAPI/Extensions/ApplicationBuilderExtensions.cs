﻿using Microsoft.AspNetCore.Builder;
using System;

namespace My_CV.WebAPI.Extensions
{
    /// <summary>
    /// Extensions of <see cref="IApplicationBuilder"/>.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Enable <see cref="RamloHttpContext.Current"/> in the current request with adding <see cref="CurrentRequestContextMiddleware"/> to the application's request pipeline.
        /// </summary>
        /// <param name="builder">The application's request pipeline.</param>
        /// <returns>The application's request pipeline added <see cref="CurrentRequestContextMiddleware"/>.</returns>
        public static IApplicationBuilder UseCurrentRequestContext(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseMiddleware<CurrentRequestContextMiddleware>();
        }
    }
}
