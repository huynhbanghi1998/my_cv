﻿using Microsoft.AspNetCore.Http;
using My_CV.WebAPI.Configuration;
using System.Threading.Tasks;

namespace My_CV.WebAPI.Extensions
{
    public class CurrentRequestContextMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Create a new instance of <see cref="CurrentRequestContextMiddleware"/>.
        /// </summary>
        /// <param name="next">The next middleware in the pipeline.</param>
        public CurrentRequestContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Execute the application's request pipeline.
        /// </summary>
        /// <param name="context"><see cref="HttpContext"/> in this request.</param>
        /// <returns>The task.</returns>
        public async Task Invoke(HttpContext context)
        {
            CoreHttpContext.Current = context;

            await _next(context);

            CoreHttpContext.Current = null;
        }
    }
}
