﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using My_CV.Service;
using My_CV.Service.Configuration;
using My_CV.Service.Constant;
using My_CV.Service.Models;
using My_CV.WebAPI.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using My_CV.WebAPI.Utilities;

namespace My_CV.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MyCVSetting>(Configuration.GetSection("MyCV_Settings"));

            MyCVHttpContext.ConnenctionString = Configuration.GetSection("MyCV_Settings:ConnectionString").Value;
            MyCVHttpContext.UploadPath = Configuration.GetSection("MyCV_Settings:UPLOAD_PATH").Value;

            MyCVHttpContext.Mail_Server = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_SERVER").Value;
            MyCVHttpContext.Mail_UserName = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_LOGIN").Value;
            MyCVHttpContext.Mail_Password = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_PASSWORD").Value;

            MyCVHttpContext.Mail_From = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_FROM").Value;
            MyCVHttpContext.Mail_To = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_TO").Value;
            MyCVHttpContext.Mail_FullName = Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_FULLNAME").Value;
            MyCVHttpContext.Mail_Port = 0;
            if (Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_PORT").Value != null)
            {
                MyCVHttpContext.Mail_Port = Int32.Parse(Configuration.GetSection("MyCV_Settings:SMTPSetting:SMTP_PORT").Value);
            }

            WebIocRegister.RegisterServices(services);

            // Build the intermediate service provider
            //services.BuildServiceProvider();


            // ===== Add Identity ========
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<MyCVContext>()
                .AddDefaultTokenProviders();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("MyCVUser", policy => policy.RequireClaim(MyCVIdentityClaims.LoginUserId));
                options.AddPolicy("MyCVAdmin", policy => policy.RequireClaim(MyCVIdentityClaims.IsAdmin));
            });

            // Add framework services.
            services.AddMvc().AddJsonOptions(a => a.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
            services.AddMvc().AddJsonOptions(a => a.SerializerSettings.Converters = new List<JsonConverter> { new DecimalConverter() });

            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptionsSnapshot<MyCVSetting> MyCV_Settings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCurrentRequestContext();
            var clientUrl = MyCV_Settings.Value.ClientUrl.Split(",");
            app.UseCors(option => option.WithOrigins(clientUrl).AllowAnyMethod().AllowAnyHeader().AllowCredentials());

            //app.UseCurrentRequestContext();
            app.UseAuthentication();

            app.MapWhen(
                c => !c.Request.Path.Value.StartsWith("/api") && !c.Request.Path.Value.StartsWith("/image"),
                b => b.Run(async c => await c.Response.WriteAsync("Welcome Admin System API")));

            // add public folder
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(MyCVHttpContext.UploadPath),
                RequestPath = "/image"
            });

            // Allow directory browse of log files
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "logs")),
                RequestPath = "/logs",
                EnableDirectoryBrowsing = true
            });

            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
