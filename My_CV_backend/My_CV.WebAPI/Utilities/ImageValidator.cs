﻿using Microsoft.AspNetCore.Http;
using System.Drawing;

namespace My_CV.WebAPI.Utilities
{
    public static class ImageValidator
    {
        public static bool IsImage(this IFormFile file)
        {
            try
            {
                var img = Image.FromStream(file.OpenReadStream());
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
