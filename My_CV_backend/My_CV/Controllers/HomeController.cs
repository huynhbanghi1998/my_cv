﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using My_CV.Models;
using My_CV.Service.ServiceContracts.Query;

namespace My_CV.Controllers
{
    public class HomeController : Controller
    {
        private readonly IInformationQueryService _informationQueryService;
        private readonly IEducationsQueryService _educationsQueryService;
        private readonly IExperiencesQueryService _experiencesQueryService;
        private readonly IRecommendationsQueryService _recommendationsQueryService;
        private readonly ISkillsQueryService _skillsQueryService;
        private readonly ILanguagesQueryService _languagesQueryService;
        private readonly IProductsQueryService _productsQueryService;

        public HomeController(IInformationQueryService informationQueryService, IEducationsQueryService educationsQueryService,
            IExperiencesQueryService experiencesQueryService, IRecommendationsQueryService recommendationsQueryService, ISkillsQueryService skillsQueryService,
            ILanguagesQueryService languagesQueryService, IProductsQueryService productsQueryService)
        {
            _informationQueryService = informationQueryService;
            _educationsQueryService = educationsQueryService;
            _experiencesQueryService = experiencesQueryService;
            _recommendationsQueryService = recommendationsQueryService;
            _skillsQueryService = skillsQueryService;
            _languagesQueryService = languagesQueryService;
            _productsQueryService = productsQueryService;
        }

        public async Task<IActionResult> Index()
        {
            var listInfor = await this._informationQueryService.GetInformationInHome();
            var listEducation = await this._educationsQueryService.GetEducationInHome();
            var listExperience = await this._experiencesQueryService.GetExperienceInHome();
            var listRecommendation = await this._recommendationsQueryService.GetRecommendationInHome();
            var listSkill = await this._skillsQueryService.GetSkillInHome();
            var listLanguage = await this._languagesQueryService.GetLanguageInHome();
            var listProduct = await this._productsQueryService.GetProductInHome();
            var listProductType = await this._productsQueryService.getProductType(); // to display in My Projects

            ViewData["HomeInfo"] = listInfor[0];
            ViewData["EducationInfo"] = listEducation;
            ViewData["ExperienceInfo"] = listExperience;
            ViewData["RecommandationInfo"] = listRecommendation;
            ViewData["SkillInfo"] = listSkill;
            ViewData["LanguageInfo"] = listLanguage;
            ViewData["ProductInfo"] = listProduct;
            ViewData["ProductType"] = listProductType;


            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel() { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}