﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using My_CV.Service;
using My_CV.Service.Configuration;

namespace My_CV
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MyCVSetting>(Configuration.GetSection("MyCV_Settings"));

            var connection = Configuration.GetSection("MyCV_Settings:ConnectionString").Value;
            MyCVHttpContext.ConnenctionString = connection;
            MyCVHttpContext.UploadPath = Configuration.GetSection("MyCV_Settings:UPLOAD_PATH").Value;
            MyCVHttpContext.ApiUrl = Configuration.GetSection("MyCV_Settings:ApiUrl").Value;

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            WebIocRegister.RegisterServices(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptionsSnapshot<MyCVSetting> myCVSettings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseCookiePolicy();

            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}");
            //});

            var clientUrl = myCVSettings.Value.ClientUrl.Split(",");
            app.UseCors(option => option.WithOrigins(clientUrl).AllowAnyMethod().AllowAnyHeader());

            app.UseStaticFiles();

            //app.UseHttpsRedirection();
            app.UseCookiePolicy();

            // add public folder
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(MyCVHttpContext.UploadPath),
                RequestPath = "/image"
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
