#pragma checksum "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "608c7083d78200c15085c6b4957256b6792bea16"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\_ViewImports.cshtml"
using My_CV;

#line default
#line hidden
#line 2 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\_ViewImports.cshtml"
using My_CV.Models;

#line default
#line hidden
#line 17 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
using My_CV.Service.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"608c7083d78200c15085c6b4957256b6792bea16", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1f64222f6e17ab32d14401e894e82935dec1d737", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("callSendMessage"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("custom-form-style"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("#"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(45, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
  
    var information = (Information)ViewData["HomeInfo"];

#line default
#line hidden
            BeginContext(112, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 9 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
  
    List<Educations> listEducation = new List<Educations>(ViewData["EducationInfo"] as List<Educations>);

#line default
#line hidden
            BeginContext(228, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 13 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
  
    List<Experiences> listExperience = ViewData["ExperienceInfo"] as List<Experiences>;

#line default
#line hidden
            BeginContext(326, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(357, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(371, 247, true);
            WriteLiteral("<section id=\"about-me\" class=\"section section-no-border section-parallax custom-section-padding-1 custom-position-1 custom-xs-bg-size-cover parallax-no-overflow m-0\" data-plugin-parallax data-plugin-options=\"{\'speed\': 1.5}\" data-image-src=\"images/");
            EndContext();
            BeginContext(619, 26, false);
#line 20 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                                                                                                                                                                  Write(information.CoverImage1Url);

#line default
#line hidden
            EndContext();
            BeginContext(645, 142, true);
            WriteLiteral("\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-4 custom-sm-margin-bottom-1\">\r\n                <img");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 787, "\"", 822, 2);
            WriteAttributeValue("", 793, "images/", 793, 7, true);
#line 24 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
WriteAttributeValue("", 800, information.AvatarUrl, 800, 22, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(823, 212, true);
            WriteLiteral(" class=\"img-fluid custom-border custom-image-position-2 custom-box-shadow-4\" />\r\n            </div>\r\n            <div class=\"col-lg-6 col-xl-5\">\r\n                <h1 class=\"text-color-primary custom-font-size-1\">");
            EndContext();
            BeginContext(1036, 16, false);
#line 27 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                             Write(information.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1052, 132, true);
            WriteLiteral("</h1>\r\n                <p class=\"text-color-light font-weight-normal custom-font-size-2 custom-margin-bottom-1\">The new graduate at ");
            EndContext();
            BeginContext(1185, 23, false);
#line 28 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                                        Write(listEducation[0].Tittle);

#line default
#line hidden
            EndContext();
            BeginContext(1208, 233, true);
            WriteLiteral("</p>\r\n                <span class=\"custom-about-me-infos\">\r\n                    <span class=\"custom-text-color-1 text-uppercase\">\r\n                        <strong class=\"text-color-light\">Previous: </strong>\r\n                        ");
            EndContext();
            BeginContext(1442, 56, false);
#line 32 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                   Write(listExperience[listExperience.Count - 1].postionWorked());

#line default
#line hidden
            EndContext();
            BeginContext(1498, 347, true);
            WriteLiteral(@"
                        <a data-hash href=""#experience"" class=""btn btn-tertiary text-uppercase custom-btn-style-1 text-1 ml-2"">View More</a>
                    </span>
                    <span class=""custom-text-color-1 text-uppercase"">
                        <strong class=""text-color-light"">Education: </strong>
                        ");
            EndContext();
            BeginContext(1846, 45, false);
#line 37 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                   Write(listEducation[listEducation.Count - 1].Tittle);

#line default
#line hidden
            EndContext();
            BeginContext(1891, 539, true);
            WriteLiteral(@"
                        <a data-hash href=""#education"" class=""btn btn-tertiary text-uppercase custom-btn-style-1 text-1 ml-2"">View More</a>
                    </span>
                </span>
            </div>
        </div>
    </div>
    <ul class=""social-icons custom-social-icons"">
        <li class=""social-icons-facebook"">
            <a href=""http://www.facebook.com/banghi9x"" target=""_blank"" title=""Facebook"">
                <i class=""fab fa-facebook-f""></i>
            </a>
        </li>
    </ul>
</section>

");
            EndContext();
            BeginContext(2442, 1024, true);
            WriteLiteral(@"<div class=""custom-about-me-links background-color-light"">
    <div class=""container"">
        <div class=""row justify-content-center"">
            <div class=""col-lg-3 text-center custom-xs-border-bottom p-0"">
                <a data-hash href=""#say-hello"" class=""text-decoration-none"">
                    <span class=""custom-nav-button text-color-dark"">
                        <i class=""icon-earphones-alt icons text-color-primary""></i>
                        Contact
                    </span>
                </a>
            </div>
            <div class=""col-lg-2 text-center custom-xs-border-bottom p-0"">
                <a data-hash href=""#say-hello"" class=""text-decoration-none"">
                    <span class=""custom-nav-button custom-divisors text-color-dark"">
                        <i class=""icon-envelope-open icons text-color-primary""></i>
                        Send Message
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

");
            EndContext();
            BeginContext(3478, 159, true);
            WriteLiteral("<section class=\"section section-no-border background-color-light m-0\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col\">\r\n");
            EndContext();
            BeginContext(3678, 70, false);
#line 83 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
               Write(await Html.PartialAsync("Partial/HomeInformation.cshtml", information));

#line default
#line hidden
            EndContext();
            BeginContext(3769, 195, true);
            WriteLiteral("\r\n                <a id=\"aboutMeMoreBtn\" class=\"btn btn-tertiary text-uppercase custom-btn-style-1 text-1\" href=\"#\">View More</a>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(3976, 398, true);
            WriteLiteral(@"<section id=""experience"" class=""section section-secondary section-no-border m-0"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col"">
                <h2 class=""text-color-quaternary text-uppercase font-weight-extra-bold"">Experience</h2>
                <section class=""timeline custom-timeline"" id=""timeline"">
                    <div class=""timeline-body"">
");
            EndContext();
#line 101 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                         foreach (var item in listExperience)
                        {
                            

#line default
#line hidden
            BeginContext(4493, 62, false);
#line 103 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                       Write(await Html.PartialAsync("Partial/HomeExperience.cshtml", item));

#line default
#line hidden
            EndContext();
#line 103 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                           
                        }

#line default
#line hidden
            BeginContext(4584, 176, true);
            WriteLiteral("                        <div class=\"timeline-bar\"></div>\r\n                    </div>\r\n                </section>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(4772, 443, true);
            WriteLiteral(@"<section id=""education"" class=""section section-no-border custom-background-color-1 m-0"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col"">
                <h2 class=""text-color-quaternary text-uppercase font-weight-extra-bold m-0"">Education</h2>
                <div class=""owl-carousel nav-bottom custom-dots-style-1 mb-0"" data-plugin-options=""{'items': 1, 'loop': false, 'dots': true, 'nav': false}"">
");
            EndContext();
#line 120 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                     for (int i = 0; i < listEducation.Count; i += 2)
                    {

#line default
#line hidden
            BeginContext(5309, 71, true);
            WriteLiteral("                        <div class=\"row\">\r\n                            ");
            EndContext();
            BeginContext(5381, 74, false);
#line 123 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                       Write(await Html.PartialAsync("Partial/HomeEducation.cshtml", @listEducation[i]));

#line default
#line hidden
            EndContext();
            BeginContext(5455, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 125 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                             if ((i + 1) < listEducation.Count)
                            {
                                

#line default
#line hidden
            BeginContext(5588, 78, false);
#line 127 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                           Write(await Html.PartialAsync("Partial/HomeEducation.cshtml", @listEducation[i + 1]));

#line default
#line hidden
            EndContext();
#line 127 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                               
                            }

#line default
#line hidden
            BeginContext(5699, 32, true);
            WriteLiteral("                        </div>\r\n");
            EndContext();
#line 130 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(5754, 86, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(5852, 113, true);
            WriteLiteral("<section id=\"skills\" class=\"section section-no-border background-color-light m-0\">\r\n    <div class=\"container\">\r\n");
            EndContext();
#line 140 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
          
            List<Skills> listSkill = new List<Skills>(ViewData["SkillInfo"] as List<Skills>);
        

#line default
#line hidden
            BeginContext(6083, 378, true);
            WriteLiteral(@"        <div class=""row align-items-center"">
            <div class=""col-md-6 col-lg-8 mb-4 mb-md-0"">
                <div class=""row"">
                    <div class=""col-lg-6"">
                        <h2 class=""text-color-quaternary text-uppercase font-weight-extra-bold"">Skills & Languages</h2>
                        <div class=""progress-bars custom-progress-bars"">
");
            EndContext();
#line 149 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                             for (int i = 0; i < listSkill.Count / 2; i++)
                            {
                                

#line default
#line hidden
            BeginContext(6601, 58, false);
#line 151 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                           Write(await Html.PartialAsync("Partial/HomeSkill", listSkill[i]));

#line default
#line hidden
            EndContext();
#line 151 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                           
                            }

#line default
#line hidden
            BeginContext(6692, 201, true);
            WriteLiteral("                        </div>\r\n                    </div>\r\n                    <div class=\"col-lg-6\">\r\n                        <div class=\"progress-bars custom-progress-bars custom-md-margin-top-1\">\r\n");
            EndContext();
#line 157 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                             for (int i = listSkill.Count / 2; i < listSkill.Count; i++)
                            {
                                

#line default
#line hidden
            BeginContext(7047, 58, false);
#line 159 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                           Write(await Html.PartialAsync("Partial/HomeSkill", listSkill[i]));

#line default
#line hidden
            EndContext();
#line 159 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                           
                            }

#line default
#line hidden
            BeginContext(7138, 462, true);
            WriteLiteral(@"                        </div>
                    </div>
                </div>
            </div>
            <div class=""col-md-6 col-lg-4"">
                <div class=""row"">
                    <div class=""col"">
                        <div class=""custom-box-details background-color-light custom-box-shadow-1"">
                            <h4 class=""text-color-dark"">Languages</h4>
                            <ul class=""custom-list-style-1 p-0"">
");
            EndContext();
#line 171 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                 foreach (var item in ViewData["LanguageInfo"] as List<Languages>)
                                {
                                    

#line default
#line hidden
            BeginContext(7772, 53, false);
#line 173 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                               Write(await Html.PartialAsync("Partial/HomeLanguage", item));

#line default
#line hidden
            EndContext();
#line 173 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                          
                                }

#line default
#line hidden
            BeginContext(7862, 181, true);
            WriteLiteral("                            </ul>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(8055, 546, true);
            WriteLiteral(@"<section id=""portfolio"" class=""section section-no-border background-color-secondary m-0"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col"">
                <h2 class=""text-color-quaternary font-weight-extra-bold text-uppercase"">My projects</h2>
                <ul class=""nav nav-pills sort-source custom-nav-sort mb-4"" data-sort-id=""portfolio"" data-option-key=""filter"">
                    <li class=""nav-item active"" data-option-value=""*""><a class=""nav-link text-dark active"" href=""#"">Show All</a></li>
");
            EndContext();
#line 192 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                     foreach (var item in ViewData["ProductType"] as List<Products>)
                    {

#line default
#line hidden
            BeginContext(8710, 65, true);
            WriteLiteral("                        <li class=\"nav-item\" data-option-value=\".");
            EndContext();
            BeginContext(8776, 19, false);
#line 194 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                            Write(item.Product_TypeID);

#line default
#line hidden
            EndContext();
            BeginContext(8795, 41, true);
            WriteLiteral("\"><a class=\"nav-link text-dark\" href=\"#\">");
            EndContext();
            BeginContext(8837, 16, false);
#line 194 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                                         Write(item.ProductType);

#line default
#line hidden
            EndContext();
            BeginContext(8853, 11, true);
            WriteLiteral("</a></li>\r\n");
            EndContext();
#line 195 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(8887, 320, true);
            WriteLiteral(@"                </ul>
                <div class=""sort-destination-loader sort-destination-loader-showing"">
                    <div class=""row image-gallery sort-destination lightbox"" data-sort-id=""portfolio"" data-plugin-options=""{'delegate': 'a.lightbox-portfolio', 'type': 'image', 'gallery': {'enabled': true}}"">
");
            EndContext();
#line 199 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                         foreach (var item in ViewData["ProductInfo"] as List<Products>)
                        {
                            

#line default
#line hidden
            BeginContext(9353, 59, false);
#line 201 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                       Write(await Html.PartialAsync("Partial/HomeProduct.cshtml", item));

#line default
#line hidden
            EndContext();
#line 201 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                        
                        }

#line default
#line hidden
            BeginContext(9441, 114, true);
            WriteLiteral("                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(9567, 559, true);
            WriteLiteral(@"<section id=""recommendations"" class=""section section-no-border background-color-primary m-0"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col"">
                <h2 class=""text-color-quaternary font-weight-extra-bold text-uppercase"">People Says</h2>
            </div>
            <div class=""col-lg-12 p-0"">
                <div class=""owl-carousel custom-dots-style-1 custom-dots-color-1 custom-dots-position-1 mb-0"" data-plugin-options=""{'items': 1, 'autoHeight': true, 'loop': false, 'nav': false, 'dots': true}"">
");
            EndContext();
#line 219 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                     foreach (var item in ViewData["RecommandationInfo"] as List<Recommendations>)
                    {
                        

#line default
#line hidden
            BeginContext(10274, 67, false);
#line 221 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                   Write(await Html.PartialAsync("Partial/HomeRecommandation.cshtml", @item));

#line default
#line hidden
            EndContext();
#line 221 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                            
                    }

#line default
#line hidden
            BeginContext(10366, 86, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
            EndContext();
            BeginContext(10464, 470, true);
            WriteLiteral(@"<div id=""say-hello"" class=""container-fluid"">
    <div class=""row"">
        <div class=""col-lg-6 p-0"">
            <section class=""section section-no-border background-color-primary h-100 m-0"">
                <div class=""row justify-content-end m-0"">
                    <div class=""col-half-section col-half-section-right mr-3"">
                        <h2 class=""text-color-quaternary text-uppercase font-weight-extra-bold"">Contact</h2>
                        ");
            EndContext();
            BeginContext(10934, 1357, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "896dbebe8f6149ee988f542f10be6d35", async() => {
                BeginContext(11012, 1272, true);
                WriteLiteral(@"
                            <div class=""form-content"">
                                <div class=""form-control-custom"">
                                    <input type=""text"" class=""form-control"" name=""callName"" placeholder=""Your Name *"" data-msg-required=""This field is required."" id=""callName"" required="""" />
                                </div>
                                <div class=""form-control-custom"">
                                    <input type=""text"" class=""form-control"" name=""callSubject"" placeholder=""Subject *"" data-msg-required=""This field is required."" id=""callSubject"" required="""" />
                                </div>
                                <div class=""form-control-custom"">
                                    <textarea maxlength=""5000"" data-msg-required=""Please enter your message."" rows=""10"" class=""form-control"" name=""message"" placeholder=""Message*"" id=""message"" required="""" aria-required=""true""></textarea>
                                </div>
                   ");
                WriteLiteral("             <input type=\"submit\" class=\"btn btn-quaternary text-color-light text-uppercase font-weight-semibold outline-none custom-btn-style-2 custom-border-radius-1\" value=\"Submit\" />\r\n                            </div>\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(12291, 194, true);
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </section>\r\n        </div>\r\n        <div class=\"col-lg-6 p-0\">\r\n            <section class=\"section section-no-border h-100 m-0\"");
            EndContext();
            BeginWriteAttribute("style", " style=\"", 12485, "\"", 12569, 6);
            WriteAttributeValue("", 12493, "background:", 12493, 11, true);
            WriteAttributeValue(" ", 12504, "url(images/", 12505, 12, true);
#line 256 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
WriteAttributeValue("", 12516, information.CoverImage2Url, 12516, 27, false);

#line default
#line hidden
            WriteAttributeValue("", 12543, ");", 12543, 2, true);
            WriteAttributeValue(" ", 12545, "background-size:", 12546, 17, true);
            WriteAttributeValue(" ", 12562, "cover;", 12563, 7, true);
            EndWriteAttribute();
            BeginContext(12570, 819, true);
            WriteLiteral(@">
                <div class=""row m-0"">
                    <div class=""col-half-section col-half-section-left ml-3"">
                        <a href=""mailto:you@domain.com"" class=""text-decoration-none"">
                            <span class=""feature-box custom-feature-box align-items-center mb-4"">
                                <span class=""custom-feature-box-icon"">
                                    <i class=""icon-envelope icons text-color-light""></i>
                                </span>
                                <span class=""feature-box-info"">
                                    <span class=""custom-label font-weight-semibold text-uppercase custom-text-color-1"">Email</span>
                                    <strong class=""font-weight-light text-color-light custom-opacity-effect-1"">");
            EndContext();
            BeginContext(13390, 17, false);
#line 266 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                          Write(information.Email);

#line default
#line hidden
            EndContext();
            BeginContext(13407, 808, true);
            WriteLiteral(@"</strong>
                                </span>
                            </span>
                        </a>
                        <a href=""tel:+1234657890"" class=""text-decoration-none"">
                            <span class=""feature-box custom-feature-box align-items-center mb-4"">
                                <span class=""custom-feature-box-icon"">
                                    <i class=""icon-phone icons text-color-light""></i>
                                </span>
                                <span class=""feature-box-info"">
                                    <span class=""custom-label font-weight-semibold text-uppercase custom-text-color-1"">Phone</span>
                                    <strong class=""font-weight-light text-color-light custom-opacity-effect-1"">");
            EndContext();
            BeginContext(14216, 17, false);
#line 277 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                          Write(information.Phone);

#line default
#line hidden
            EndContext();
            BeginContext(14233, 820, true);
            WriteLiteral(@"</strong>
                                </span>
                            </span>
                        </a>
                        <a href=""skype:yourskype?chat"" class=""text-decoration-none"">
                            <span class=""feature-box custom-feature-box align-items-center mb-4"">
                                <span class=""custom-feature-box-icon"">
                                    <i class=""icon-social-skype icons text-color-light""></i>
                                </span>
                                <span class=""feature-box-info"">
                                    <span class=""custom-label font-weight-semibold text-uppercase custom-text-color-1"">Skype</span>
                                    <strong class=""font-weight-light text-color-light custom-opacity-effect-1"">");
            EndContext();
            BeginContext(15054, 17, false);
#line 288 "F:\MyCV_project\src\My_CV_backend\My_CV\Views\Home\Index.cshtml"
                                                                                                          Write(information.Skype);

#line default
#line hidden
            EndContext();
            BeginContext(15071, 1056, true);
            WriteLiteral(@"</strong>
                                </span>
                            </span>
                        </a>
                        <span class=""feature-box custom-feature-box align-items-center"">
                            <span class=""custom-feature-box-icon"">
                                <i class=""icon-share icons text-color-light""></i>
                            </span>
                            <a href=""http://www.facebook.com/banghi9x"" class=""d-flex text-decoration-none"">
                                <span class=""feature-box-info"">
                                    <span class=""custom-label font-weight-semibold text-uppercase custom-text-color-1"">Follow me</span>
                                    <strong class=""font-weight-light text-color-light custom-opacity-effect-1"">Facebook</strong>
                                </span>
                            </a>
                        </span>
                    </div>
                </div>
            </section>
  ");
            WriteLiteral("      </div>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
